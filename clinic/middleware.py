from django.http import HttpResponseRedirect
from django.conf import settings
from django.http import HttpResponseForbidden
from bson.json_util import ObjectId
from core import cursor

import re
import logging

logger = logging.getLogger(__name__)


class LoginRequiredMiddleware(object):
    def process_request(self, request):
        if not request.user.is_authenticated():
            EXEMPT_URLS = [re.compile(settings.LOGIN_URL.lstrip('/'))]
            if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
                EXEMPT_URLS += [re.compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]

            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect(settings.LOGIN_URL)


class ACLMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):

        if not request.user.is_authenticated():
            return

        if view_func.func_name in getattr(settings, 'ACL_EXEMPT_VIEWS', set()):
            return

        _id = request.user.userBox.user_type
        acl = cursor.acl.find_one({'_id': ObjectId(_id)})

        if not (acl and view_func.func_name in acl.get('views', [])):
            return HttpResponseForbidden("Forbidden")


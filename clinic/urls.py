from django.conf.urls import url, include

urlpatterns = [
    url(r'^', include('core.urls')),
    url(r'^medicine/', include('medicine.urls')),
    url(r'^finance/', include('finance.urls')),
    url(r'^user/', include('users.urls')),
    url(r'^patient/', include('patients.urls')),
    url(r'^acl/', include('acl.urls')),
    url(r'^ward/', include('ward.urls')),
    url(r'^nurse_driver/', include('nurse_driver.urls')),
    url(r'^reporting/', include('reporting.urls'))
]
handler404 = 'clinic.views.page_not_found_view'
handler500 = 'clinic.views.page_error_view'
handler403 = 'clinic.views.page_forbidden_view'

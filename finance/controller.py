from core import cursor
from bson.objectid import ObjectId
import datetime
from random import randint
from core.static import CREATED_DATE, CODE, ID_


class FinanceMedicineController(object):
    def __init__(self):
        self.db = cursor.financeM

    def get_all_finance(self):
        return self.db.find().sort(CREATED_DATE, -1)

    def find_finances(self, criteria, projection):
        return self.db.find(criteria, projection)

    def insert_finance(self, data):
        data[CREATED_DATE] = datetime.datetime.now()
        data[CODE] = randint(100000, 999999)
        return self.db.insert(data)

    def delete_finance_by_id(self, id_finance):
        self.db.remove({ID_: ObjectId(id_finance)})

    def find_finance_by_id(self, id_finance, projection):
        return self.db.find_one(
            {ID_: ObjectId(id_finance)},
            projection
        )


class FinanceVisitController(object):
    def __init__(self):
        self.db = cursor.financeV

    def get_all_finance(self):
        return self.db.find().sort(CREATED_DATE, -1)

    def find_finances(self, criteria, projection):
        return self.db.find(criteria, projection)

    def insert_finance(self, data):
        data[CREATED_DATE] = datetime.datetime.now()
        data[CODE] = randint(10000000, 99999999)
        return self.db.insert(data)

    def delete_finance_by_id(self, id_finance):
        self.db.remove({ID_: ObjectId(id_finance)})

    def find_finance_by_id(self, id_finance):
        try:
            return self.db.find_one(
                {ID_: ObjectId(id_finance)},
                {ID_: 0, 'patient.id': 0}
            )
        except:
            return False

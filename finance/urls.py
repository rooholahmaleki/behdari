from django.conf.urls import url
from finance.views import finances_medicine, delete_finance_medicine, find_finance_medicine, \
    add_finance_medicine, print_finance_medicine, add_medicine, delete_medicine, \
    medicine_autocomplete, patient_autocomplete, chart_finance_medicine, \
    add_finance_visit, finances_visit, delete_finance_visit, find_finance_visit, print_finance_visit, load_sub_ward

urlpatterns = [
    url(r'^finances/medicine/$', finances_medicine, name='finances_medicine'),
    url(r'^delete/finance/medicine/$', delete_finance_medicine, name='delete_finance_medicine'),
    url(r'^find/finance/medicine/$', find_finance_medicine, name='find_finance_medicine'),
    url(r'^add/finance/medicine/$', add_finance_medicine, name='add_finance_medicine'),
    url(r'^print/finance/medicine/(?P<id_medicine>[\w\d]+)/$', print_finance_medicine, name='print_finance_medicine'),

    url(r'^add/medicine/$', add_medicine, name='add_medicine_basket'),  # ezafe kardane daro be sabade kharid
    url(r'^delete/medicine/$', delete_medicine, name='delete_medicine_basket'),
    url(r'^autocomplete/medicine/$', medicine_autocomplete, name='medicine_autocomplete'),
    url(r'^autocomplete/patient/$', patient_autocomplete, name='patient_autocomplete'),
    url(r'^chart/medicine/$', chart_finance_medicine, name='chart_finance_medicine'),

    url(r'^add/finance/visit/$', add_finance_visit, name='add_finance_visit'),
    url(r'^finances/visit/$', finances_visit, name='finances_visit'),
    url(r'^delete/finance/visit/$', delete_finance_visit, name='delete_finance_visit'),
    url(r'^find/finance/visit/$', find_finance_visit, name='find_finance_visit'),
    url(r'^print/finance/visit/(?P<visit_id>[\w\d]+)/$', print_finance_visit, name='print_finance_visit'),

    url(r'^load/sub_ward/$', load_sub_ward, name='load_sub_ward'),
]

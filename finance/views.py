import uuid
from django.shortcuts import render
from django.http import JsonResponse
from ward.controller import WardController
from ward.controller import QueueController
from medicine.controller import MedicineController
from patients.controller import PatientsController
from django.views.decorators.http import require_POST
from controller import FinanceMedicineController, FinanceVisitController
from django.conf import settings
from core.static import FINANCES, ID_FINANCE, ID_FINANCE_MEDICINE, \
    ID_, LIST_MEDICINE, POST, SUCCESS, ERROR, EMPTY_FACTOR, MESSAGE, \
    ID_MEDICINE, COUNT, TOTAL_AMOUNT, SUM_PRICE, RECEIVED_PRICE, PRICE, ID, EN_NAME, \
    SELLER, FA_NAME, NAME, PATIENT, SUB_WARD, ID_PATIENT, GUEST, PATIENT_NAME, \
    FULL_NAME, MEDICINE, L_NAME, F_NAME, NATIONAL_CODE, COUNT_MEDICINE, WRONG_RECEIVED_PRICE, \
    LIST_WARD, WARDS, ID_VISIT, ID_FINANCE_VISIT, ID_MEDICINE_, FINANCE_, VTA_STATUS, \
    SUM_VTA_PRICE, VTA_PERCENT, LIST_SUB_WARD, ID_WARD, ID_SUB_WARD, COUNT_SUB_WARD,\
    WARD_NAME, SUB_WARD_NAME, NUMBER, FINANCE_VISIT, FINANCE_MEDICINE


def finances_medicine(request):
    args = {
        FINANCES: FinanceMedicineController().get_all_finance()
    }
    return render(request, 'finance/finances_medicine.html', args)


@require_POST
def delete_finance_medicine(request):
    id_finance_medicine = request.POST[ID_FINANCE]
    FinanceMedicineController().delete_finance_by_id(id_finance_medicine)
    return JsonResponse({})


@require_POST
def find_finance_medicine(request):
    id_finance_medicine = request.POST[ID_FINANCE_MEDICINE]
    projection = {LIST_MEDICINE: 1, ID_: 0}
    list_medicine = FinanceMedicineController().find_finance_by_id(id_finance_medicine, projection)
    if list_medicine:
        list_medicine = {LIST_MEDICINE: list_medicine}
    return JsonResponse(list_medicine)


def add_finance_medicine(request):
    if request.method == POST:
        finance_factor = get_dict_finance(request)
        id_medicine = ""
        if finance_factor[LIST_MEDICINE] or request.POST[RECEIVED_PRICE].isdigit():
            id_medicine = str(FinanceMedicineController().insert_finance(finance_factor))
            if id_medicine:
                remove_id_medicine_from_session(request)
                message = SUCCESS
            else:
                message = ERROR
        else:
            message = EMPTY_FACTOR
        return JsonResponse({MESSAGE: message, ID_MEDICINE: id_medicine})

    else:
        remove_id_medicine_from_session(request)
        return render(request, 'finance/add_finance_medicine.html')


def get_dict_finance(request):
    list_medicine = get_list_medicine(request)
    sum_price = 0
    for item in list_medicine:
        item[TOTAL_AMOUNT] = item[PRICE] * item[COUNT]
        sum_price += item[TOTAL_AMOUNT]
        MedicineController().decrease_medicine_count(item[ID], item[COUNT])

    dict_finance = {
        LIST_MEDICINE: list_medicine,
        SUM_PRICE: sum_price,
        RECEIVED_PRICE: int(request.POST[RECEIVED_PRICE])
    }
    append_data_buyer(request, dict_finance)
    append_data_user_created(request, dict_finance)
    return dict_finance


def append_data_user_created(request, dict_finance):
    dict_finance[SELLER] = {}
    dict_finance[SELLER][NAME] = request.user.userBox.full_name
    dict_finance[SELLER][ID] = request.user.userBox.mongo_id


def append_data_buyer(request, dict_finance):
    dict_finance[PATIENT] = {}
    if request.POST[ID_PATIENT] == GUEST:
        dict_finance[PATIENT][NAME] = request.POST[PATIENT_NAME]
        dict_finance[PATIENT][ID] = GUEST
    else:
        try:
            patient = PatientsController().find_patient_by_id(request.POST[ID_PATIENT])
            if patient:
                dict_finance[PATIENT][NAME] = patient[FULL_NAME]
                dict_finance[PATIENT][ID] = patient[ID_]
                dict_finance[PATIENT][NATIONAL_CODE] = patient[NATIONAL_CODE]
        except:
            dict_finance[PATIENT][NAME] = request.POST[PATIENT_NAME]
            dict_finance[PATIENT][ID] = GUEST


def get_list_medicine(request):
    list_medicine = []
    list_key = list(request.session.keys())
    startswith = FINANCE_ + request.user.userBox.mongo_id
    for item in list_key:
        if item.startswith(startswith):
            list_medicine.append(request.session[item])
    return list_medicine


def remove_id_medicine_from_session(request):
    list_key = list(request.session.keys())
    startswith_f = FINANCE_ + request.user.userBox.mongo_id
    startswith_m = ID_MEDICINE_ + request.user.userBox.mongo_id
    for item in list_key:
        if item.startswith(startswith_m) or item.startswith(startswith_f):
            del request.session[item]


def medicine_autocomplete(request):
    medicine = request.GET[MEDICINE]
    list_medicine = MedicineController().find_medicine_by_regex(medicine)
    medicines = []
    for item in list_medicine:
        dict_medicine = {
            EN_NAME: item[EN_NAME],
            FA_NAME: item[FA_NAME],
            PRICE: item[PRICE],
            COUNT: item[COUNT],
            ID: str(item[ID_]),
        }
        medicines.append(dict_medicine)

    return JsonResponse(medicines, safe=False)


def patient_autocomplete(request):
    patient = request.GET[PATIENT]
    list_patient = PatientsController().find_patient_by_regex(patient)
    patients = []
    for item in list_patient:
        dict_medicine = {
            F_NAME: item[F_NAME],
            L_NAME: item[L_NAME],
            FULL_NAME: item[FULL_NAME],
            NATIONAL_CODE: item[NATIONAL_CODE],
            ID: str(item[ID_])
        }
        patients.append(dict_medicine)
    return JsonResponse(patients, safe=False)


@require_POST
def add_medicine(request):
    response = {}
    mongo_id = request.user.userBox.mongo_id
    id_medicine = request.POST[ID_MEDICINE]
    count_medicine = int(request.POST[COUNT_MEDICINE])
    medicine = MedicineController().find_medicine_by_id(id_medicine)

    if ID_MEDICINE_ + mongo_id + id_medicine in request.session.keys():
        medicine[COUNT] = request.session[ID_MEDICINE_ + mongo_id + id_medicine]

    if count_medicine <= medicine[COUNT]:
        response[EN_NAME] = medicine[EN_NAME]
        response[PRICE] = medicine[PRICE]
        response[ID] = id_medicine
        response[COUNT] = count_medicine
        request.session[ID_MEDICINE_ + mongo_id + id_medicine] = medicine[COUNT] - count_medicine
        request.session[FINANCE_ + mongo_id + str(uuid.uuid4())] = response
        response[MESSAGE] = SUCCESS
    else:
        response[MESSAGE] = ERROR

    return JsonResponse(response)


@require_POST
def delete_medicine(request):
    mongo_id = request.user.userBox.mongo_id
    id_medicine = request.POST[ID_MEDICINE]
    count_medicine = int(request.POST[COUNT_MEDICINE])
    medicine = MedicineController().find_medicine_by_id(id_medicine)
    if ID_MEDICINE_ + mongo_id + id_medicine in request.session.keys():
        count_session = request.session[ID_MEDICINE_ + mongo_id + id_medicine]
        if count_medicine + count_session <= medicine[COUNT]:
            request.session[ID_MEDICINE_ + mongo_id + id_medicine] = count_session + count_medicine

    return JsonResponse({PRICE: medicine[PRICE]})


def finances_visit(request):
    args = {
        FINANCES: FinanceVisitController().get_all_finance()
    }
    return render(request, 'finance/finances_visit.html', args)


def add_finance_visit(request):
    if request.method == POST:
        message = WRONG_RECEIVED_PRICE
        id_visit = 0
        if request.POST[RECEIVED_PRICE].isdigit():
            data = get_dict_visit(request)
            if data:
                id_visit = FinanceVisitController().insert_finance(data)
                message = SUCCESS
            else:
                message = EMPTY_FACTOR

        return JsonResponse({MESSAGE: message, ID_VISIT: str(id_visit)})
    else:
        args = {
            WARDS: WardController().get_all_ward()
        }
        return render(request, 'finance/add_finance_visit.html', args)


def load_sub_ward(request):
    sub_ward = []
    find_ward = WardController().find_ward_by_id(request.POST[ID_WARD])
    if find_ward:
        sub_ward = find_ward[SUB_WARD]
    return JsonResponse({SUB_WARD: sub_ward})


def get_dict_visit(request):
    list_ward = get_list_ward_visit(request)
    if len(list_ward) > 0:
        sum_price = 0
        sum_vta_price = 0
        for item in list_ward:
            price = (int(item[PRICE]) * int(item[COUNT]))
            sum_price += price
            vta_price = 0
            if item[VTA_STATUS]:
                vta_price = ((price * settings.VAT) / 100)
                sum_vta_price += vta_price
            item[TOTAL_AMOUNT] = price + vta_price
            item[VTA_PERCENT] = settings.VAT

        dict_finance = {
            LIST_WARD: list_ward,
            SUM_PRICE: sum_price,
            SUM_VTA_PRICE: sum_vta_price,
            TOTAL_AMOUNT: sum_price + sum_vta_price,
            RECEIVED_PRICE: int(request.POST[RECEIVED_PRICE])
        }
        append_data_buyer(request, dict_finance)
        append_data_user_created(request, dict_finance)
    else:
        dict_finance = False
    return dict_finance


def get_list_ward_visit(request):
    list_sub_ward = eval(request.POST[LIST_SUB_WARD])
    list_sub_ward_final = []
    for item in list_sub_ward:
        dict_sub = {}
        find_ward = WardController().find_ward_by_id_and_sub_id(item[ID_WARD], item[ID_SUB_WARD])
        if find_ward:
            dict_sub[COUNT] = item[COUNT_SUB_WARD]
            dict_sub[ID_WARD] = item[ID_WARD]
            dict_sub[ID_SUB_WARD] = item[ID_SUB_WARD]
            dict_sub[WARD_NAME] = find_ward[NAME]
            dict_sub[SUB_WARD_NAME] = find_ward[SUB_WARD][0][NAME]
            dict_sub[PRICE] = find_ward[SUB_WARD][0][PRICE]
            dict_sub[VTA_STATUS] = find_ward[SUB_WARD][0][VTA_STATUS]
            queue = QueueController(item[ID_SUB_WARD])
            dict_sub[NUMBER] = queue.return_queue()
            list_sub_ward_final.append(dict_sub)

    return list_sub_ward_final


@require_POST
def delete_finance_visit(request):
    id_finance = request.POST[ID_FINANCE]
    FinanceVisitController().delete_finance_by_id(id_finance)
    return JsonResponse({})


@require_POST
def find_finance_visit(request):
    id_finance_visit = request.POST[ID_FINANCE_VISIT]
    list_ward = FinanceVisitController().find_finance_by_id(id_finance_visit)
    if list_ward:
        list_ward = {LIST_WARD: list_ward[LIST_WARD]}
    return JsonResponse(list_ward)


def print_finance_visit(request, visit_id):
    finance_visit = FinanceVisitController().find_finance_by_id(visit_id)
    if finance_visit:
        return render(request, 'finance/print_finance_visit.html', {FINANCE_VISIT: finance_visit})


def chart_finance_medicine(request):
    return render(request, 'finance/chart_medicine_finance.html')


def print_finance_medicine(request, id_medicine):
    finance_medicine = FinanceMedicineController().find_finance_by_id(id_medicine, {ID_: 0})
    if finance_medicine:
        return render(request, 'finance/print_finance_medicine.html', {FINANCE_MEDICINE: finance_medicine})

# coding: utf-8

from collections import OrderedDict

acl_visible_segment = OrderedDict()

acl_visible_segment["users_management"] = u'مدیریت کاربران'
acl_visible_segment["patient_management"] = u'مدیریت بیماران'
acl_visible_segment["ward_management"] = u'مدیریت بخش ها'
acl_visible_segment["medicine_management"] = u'مدیریت داروها'
acl_visible_segment["medicine_factor_management"] = u'مدیریت فاکتورهای دارو'
acl_visible_segment["visit_factor_management"] = u'مدیریت فاکتورهای خدمات'
acl_visible_segment["delete_management"] = u'دسترسی حذف'
acl_visible_segment["nurse_driver_management"] = u'مدیریت بهیار ها و راننده ها'
acl_visible_segment["reporting_management"] = u'مدیریت گزارش ها'

acl_view_segment = {
    "users_management": [
        "groups",
        "define_group",
        "edit_group",
        "users",
        "add_user",
        "edit_user"
    ],
    "patient_management": [
        "patients",
        "add_patient",
        "edit_patient"
    ],
    "ward_management": [
        "wards",
        "add_ward",
        "add_sub_ward",
        "load_sub_ward"
    ],
    "medicine_management": [
        "medicines",
        "add_medicine",
        "add_count_medicine",
        "edit_medicine",
    ],
    "medicine_factor_management": [
        "finances_medicine",
        "find_finance_medicine",
        "add_finance_medicine",
        "add_medicine_basket",
        "delete_medicine_basket",
        "medicine_autocomplete",
        "patient_autocomplete",
    ],
    "visit_factor_management": [
        "add_finance_visit",
        "finances_visit",
        "find_finance_visit",
        "print_finance_visit",
        "print_finance_medicine",
        "patient_autocomplete",
        "load_sub_ward",
    ],
    "delete_management": [
        "delete_patient",
        "delete_ward",
        "delete_sub_ward",
        "delete_medicine",
        "delete_finance_medicine",
        "delete_finance_visit",
        "delete_user",
        "delete_nurse_or_driver",
    ],
    "nurse_driver_management": [
        'nurses_drivers',
        'nurse_and_driver_calendar',
        'add_nurse_and_driver_calendar',
        'find_nurse_and_driver_calendar',
    ],
    "reporting_management": [
        "date_reporting",
        "reporting_visit",
        "reporting_medicine",
    ]
}
# coding: utf-8
from django import forms
from acl.utils import acl_visible_segment
from acl.utils import acl_view_segment
from core import psword
from core.static import VISIBLE_TEMPLATETAGS, NAME, VIEWS


class GroupForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

    name = forms.CharField(
        max_length=100,
        error_messages={'required': psword.GROUP_MESSAGE},
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': psword.EN_NAME})
    )
    visible_templatetags = forms.MultipleChoiceField(
        choices=acl_visible_segment.items(),
        required=False,
        widget=forms.CheckboxSelectMultiple(attrs={'class': 'md-check'})
    )

    def clean(self, *args, **kwargs):
        form_data = super(GroupForm, self).clean()
        visible_templatetags = form_data.get(VISIBLE_TEMPLATETAGS)
        form_data_clean = {
            VISIBLE_TEMPLATETAGS: visible_templatetags,
            NAME: form_data.get(NAME),
            VIEWS: []
        }
        for template in visible_templatetags:
            form_data_clean[VIEWS].extend(acl_view_segment.get(template, []))

        self.cleaned_data = form_data_clean

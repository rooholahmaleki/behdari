# -*- coding:utf-8 -*-
from bson.objectid import ObjectId
from django.conf import settings
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern

from core import cursor
from core.static import ACL_EXEMPT_VIEWS


def insert_acl_group(doc):
    result = cursor.acl_group.insert(doc)
    if ObjectId.is_valid(result):
        return True
    else:
        return False


def group_query(data, _id):
    criteria = {'_id': ObjectId(_id)}
    update = cursor.acl_group.find_one_and_replace(criteria, data)
    return update


def get_all_view_names(urlpatterns=None):
    if not urlpatterns:
        global views_name
        views_name = set()
        root_urlconf = __import__(settings.ROOT_URLCONF)
        urlpatterns = root_urlconf.urls.urlpatterns

    for pattern in urlpatterns:
        if isinstance(pattern, RegexURLResolver):
            get_all_view_names(pattern.url_patterns)
        elif isinstance(pattern, RegexURLPattern):
            view_name = pattern.callback.func_name
            views_name.add(view_name)

    return sorted(list(
        views_name.difference(getattr(settings, ACL_EXEMPT_VIEWS, set()))
    ))


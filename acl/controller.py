from core import cursor
from bson.json_util import ObjectId


class AclController(object):
    def __init__(self):
        self.db = cursor.acl

    def insert_acl_group(self, doc):
        result = self.db.insert(doc)
        if ObjectId.is_valid(result):
            return True
        else:
            return False

    def get_all_group_name(self):
        return self.db.find()

    def find_group_by_name(self, name):
        return self.db.find_one({"name": name})

    def find_group_by_id(self, _id):
        return self.db.find_one({"_id": ObjectId(_id)}, {"_id": 0})

    def update_group(self, _id, data):
        criteria = {'_id': ObjectId(_id)}
        update = self.db.find_one_and_replace(criteria, data)
        return update

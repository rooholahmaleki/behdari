from acl.views import define_group, groups, edit_group
from django.conf.urls import url


urlpatterns = [
    url(r'^groups/$', groups, name='groups'),
    url(r'^define_group/$', define_group, name='define_group'),
    url(r'^edit_group/(?P<group_id>[\w\d]+)$', edit_group, name='edit_group'),
]


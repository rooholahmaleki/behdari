from django.shortcuts import render, redirect

from acl.controller import AclController
from acl.form import GroupForm
from core.static import ERRORS, FORM, POST, GROUPS
from core import psword


def define_group(request):
    args = {}
    if request.method == POST:
        form = GroupForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            insert = AclController().insert_acl_group(data)
            if insert:
                return redirect(groups)
            else:
                args[ERRORS] = psword.INSERT_ERROR
        else:
            args[ERRORS] = form.errors

    args[FORM] = GroupForm()
    return render(request, 'acl/define_group.html', args)


def groups(request):
    args = {GROUPS: AclController().get_all_group_name()}
    return render(request, 'acl/groups.html', args)


def edit_group(request, group_id):
    args = {}
    initial = AclController().find_group_by_id(group_id)
    if request.POST:
        form = GroupForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            update = AclController().update_group(group_id, data)
            if update:
                return redirect(groups)
            else:
                args[ERRORS] = psword.INSERT_ERROR
        else:
            args[ERRORS] = form.errors

    args[FORM] = GroupForm(initial)

    return render(request, 'acl/edit_group.html', args)

from __future__ import unicode_literals

from django.apps import AppConfig


class NurseDriverConfig(AppConfig):
    name = 'nurse_driver'

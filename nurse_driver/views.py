from django.http import JsonResponse
from django.shortcuts import render
from controller import NurseDriverController, NurseDriverCalendarController
from form import NurseDriverForm, CalendarNurseDriverForm
from django.shortcuts import redirect
from core import psword
from core.static import NURSES, DRIVERS, FORM, POST, ERRORS, ERROR, UPDATE,\
    DATE, MESSAGE, SUCCESS, USERS, DRIVER, NURSE, INSERT


def nurses_drivers(request):
    args = {
        NURSES: NurseDriverController().get_all_nurse(),
        DRIVERS: NurseDriverController().get_all_driver(),
        FORM: NurseDriverForm()
    }
    if request.method == POST:
        form = NurseDriverForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            insert = NurseDriverController().insert(data)
            if insert:
                return redirect(nurses_drivers)
            else:
                args[ERRORS] = psword.INSERT_ERROR

        else:
            args[ERRORS] = form.errors

    return render(request, 'nurse_driver/nurses_drivers.html', args)


def delete_nurse_or_driver(request, d_id):
    NurseDriverController().delete(d_id)
    return redirect(nurses_drivers)


def nurse_and_driver_calendar(request):
    args = {
        FORM: CalendarNurseDriverForm()
    }
    return render(request, 'nurse_driver/calendar.html', args)


def add_nurse_and_driver_calendar(request):
    form = CalendarNurseDriverForm(request.POST)
    message = ERROR
    if form.is_valid():
        data = form.cleaned_data
        calendar = NurseDriverCalendarController().find_one(data[DATE])
        if calendar:
            update = NurseDriverCalendarController().update(data, calendar)
            if update:
                message = UPDATE
        else:
            insert = NurseDriverCalendarController().insert(data)
            if insert:
                message = INSERT
    return JsonResponse({MESSAGE: message})


def find_nurse_and_driver_calendar(request):
    date = request.POST.get(DATE)
    result = NurseDriverCalendarController().find_one(date)
    message = ERROR
    users = []
    if result:
        users = NurseDriverController().find([result[DRIVER], result[NURSE]])
        message = SUCCESS
    return JsonResponse({MESSAGE: message, USERS: users})

import datetime
from core import cursor
from bson.objectid import ObjectId
from core.static import CREATED_DATE, TYPE, NURSE, STATUS, DRIVER,\
    ID_, HISTORY, DATE, LAST_UPDATE


class NurseDriverController(object):
    def __init__(self):
        self.db = cursor.nurse_driver

    def get_all_nurse(self):
        return self.db.find({TYPE: NURSE, STATUS: True}).sort(CREATED_DATE, -1)

    def get_all_driver(self):
        return self.db.find({TYPE: DRIVER, STATUS: True}).sort(CREATED_DATE, -1)

    def insert(self, data):
        data[CREATED_DATE] = datetime.datetime.now()
        data[STATUS] = True
        return self.db.insert(data)

    def delete(self, _id):
        return self.db.update_one({ID_: ObjectId(_id)}, {"$set": {STATUS: False}})

    def find(self, list_id):
        return list(self.db.find(
            {ID_: {"$in": [ObjectId(_id) for _id in list_id]}},
            {ID_: 0}
        ))


class NurseDriverCalendarController(object):
    def __init__(self):
        self.db = cursor.n_d_calendar

    def insert(self, data):
        data[CREATED_DATE] = datetime.datetime.now()
        return self.db.insert(data)

    def find_one(self, date):
        return self.db.find_one({DATE: date})

    def update(self, new, old):
        history = old.get(HISTORY, [])
        history.extend([new[DRIVER], new[NURSE]])
        return self.db.update_one(
            {DATE: old[DATE]},
            {"$set":
                {
                    DRIVER: new[DRIVER],
                    NURSE: new[NURSE],
                    LAST_UPDATE: datetime.datetime.now(),
                    HISTORY: history
                }
            }
        )

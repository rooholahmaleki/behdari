from nurse_driver.views import nurses_drivers, delete_nurse_or_driver,\
    nurse_and_driver_calendar, add_nurse_and_driver_calendar, find_nurse_and_driver_calendar
from django.conf.urls import url

urlpatterns = [
    url(r'^nurses_drivers/$', nurses_drivers, name='nurses_drivers'),
    url(r'^delete_nurse_or_driver/(?P<d_id>[\w\d]+)$', delete_nurse_or_driver, name='delete_nurse_or_driver'),
    url(r'^calendar/$', nurse_and_driver_calendar, name='nurse_and_driver_calendar'),
    url(r'^calendar/add/$', add_nurse_and_driver_calendar, name='add_nurse_and_driver_calendar'),
    url(r'^calendar/find/$', find_nurse_and_driver_calendar, name='find_nurse_and_driver_calendar'),
]


# coding: utf-8

from django import forms
from controller import NurseDriverController
from core.static import DRIVER, NURSE, F_NAME, L_NAME, ID_
from core import psword


class NurseDriverForm(forms.Form):
    USER_TYPE = (
        (NURSE, psword.NURSE),
        (DRIVER, psword.DRIVER)
    )

    def __init__(self, *args, **kwargs):
        super(NurseDriverForm, self).__init__(*args, **kwargs)

    f_name = forms.CharField(
        max_length=100,
        label=psword.F_NAME,
        error_messages={'required': psword.F_NAME_MESSAGE},
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': psword.F_NAME_MESSAGE})
    )
    l_name = forms.CharField(
        max_length=100,
        error_messages={'required': psword.L_NAME_MESSAGE},
        label=psword.L_NAME,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': psword.L_NAME_MESSAGE})
    )
    phone = forms.CharField(
        max_length=100,
        label=psword.PHONE,
        error_messages={'required': psword.PHONE_MESSAGE},
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': psword.PHONE})
    )
    type = forms.ChoiceField(
        error_messages={'required': psword.USER_TYPE_MESSAGE},
        choices=USER_TYPE,
        label=psword.USER_TYPE,
        widget=forms.Select(attrs={'class': 'form-control'})
    )


class CalendarNurseDriverForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CalendarNurseDriverForm, self).__init__(*args, **kwargs)
        self.fields[DRIVER] = forms.ChoiceField(
            widget=forms.RadioSelect,
            label=psword.DRIVER,
            choices=get_driver_choice(),
            required=True
        )
        self.fields[NURSE] = forms.ChoiceField(
            widget=forms.RadioSelect,
            label=psword.NURSE,
            choices=get_nurse_choice(),
            required=True
        )

    date = forms.CharField(
        widget=forms.HiddenInput(),
        required=True
    )


def get_driver_choice():
    drivers = NurseDriverController().get_all_driver()
    _drivers = []
    for driver in drivers:
        _drivers.append((driver[ID_], driver[F_NAME] + " " + driver[L_NAME]))
    return set(_drivers)


def get_nurse_choice():
    nurses = NurseDriverController().get_all_nurse()
    _nurses = []
    for nurse in nurses:
        _nurses.append((nurse[ID_], nurse[F_NAME] + " " + nurse[L_NAME]))
    return set(_nurses)

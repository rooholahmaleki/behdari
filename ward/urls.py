from ward.views import wards, add_ward, delete_ward, add_sub_ward, delete_sub_ward, load_sub_ward
from django.conf.urls import url

urlpatterns = [
    url(r'^wards/$', wards, name='wards'),
    url(r'^ward/add/$', add_ward, name='add_ward'),
    url(r'^ward/delete/$', delete_ward, name='delete_ward'),
    url(r'^ward/add_sub/$', add_sub_ward, name='add_sub_ward'),
    url(r'^ward/delete_sub/$', delete_sub_ward, name='delete_sub_ward'),
    url(r'^ward/load_sub_ward/$', load_sub_ward, name='load_sub_ward'),
]


from datetime import date, datetime
from core import cursor
from random import randint
from bson.objectid import ObjectId
from pymongo import ReturnDocument


class WardController(object):
    def __init__(self):
        self.db = cursor.ward

    def get_all_ward(self):
        return self.db.find().sort('created_date', -1)

    def insert_ward(self, data):
        data['created_date'] = datetime.now()
        data['sub_ward'] = []
        return self.db.insert(data)

    def delete_ward_by_id(self, id_ward):
        self.db.remove({'_id': ObjectId(id_ward)})

    def find_ward_by_id(self, id_ward):
        return self.db.find_one({'_id': ObjectId(id_ward)})

    def find_ward_by_id_and_sub_id(self, id_ward, id_sub_ward):
        return self.db.find_one(
            {'_id': ObjectId(id_ward), 'sub_ward.id': int(id_sub_ward)},
            {'name': 1, 'sub_ward.$': 1}
        )

    def insert_sub_ward(self, id_ward, data):
        while True:
            data['id'] = randint(1000000, 99999999)
            find = self.db.find_one({'sub_ward.id': data['id']})
            if not find:
                break

        self.db.update_one(
            {'_id': ObjectId(id_ward)},
            {"$push": {"sub_ward": data}}
        )

    def update_sub_ward(self, id_ward, data):
        self.db.update_one(
            {'_id': ObjectId(id_ward), 'sub_ward.id': data['id']},
            {"$set":
                {
                    'sub_ward.$.name': data['name'],
                    'sub_ward.$.price': data['price'],
                    'sub_ward.$.vta_status': data['vta_status'],
                }
            }
        )

    def delete_sub_ward(self, id_ward, id_sub):
        self.db.update_one(
            {'_id': ObjectId(id_ward), 'sub_ward.id': int(id_sub)},
            {"$pull": {"sub_ward": {"id": int(id_sub)}}}
        )

    def update_ward(self, id_ward, name):
        self.db.update_one(
            {'_id': ObjectId(id_ward)},
            {"$set": {"name": name}}
        )


class QueueController(object):
    def __init__(self, id_sub_ward):
        self.db = cursor.queue
        self.id_sub_ward = int(id_sub_ward)
        self.sub_ward = self.db.find_one({'id_sub_ward': self.id_sub_ward})

    def return_queue(self):
        if self.sub_ward:
            number = self.update_queue()
        else:
            number = self.insert_queue()
        return number

    def insert_queue(self):
        today = datetime.combine(date.today(), datetime.min.time())
        data = {'date': today, 'number': 1, 'id_sub_ward': self.id_sub_ward}
        self.db.insert_one(data)
        return 1

    def update_queue(self):
        today = datetime.combine(date.today(), datetime.min.time())
        if today > self.sub_ward['date']:
            data = {'$set': {'date': today, 'number': 1}}
        else:
            data = {'$inc': {'number': 1}}

        queue = self.db.find_one_and_update({'id_sub_ward': self.id_sub_ward}, data, return_document=ReturnDocument.AFTER)
        if queue:
            return queue["number"]
        else:
            return 1

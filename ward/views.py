#! -*- coding: utf-8 -*-
# coding: utf-8
from random import randint

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET
from controller import WardController
from django.conf import settings


@require_GET
def wards(request):
    args = {
        "wards": WardController().get_all_ward()
    }
    return render(request, 'wards.html', args)


@require_POST
def delete_ward(request):
    id_ward = request.POST['id_ward']
    WardController().delete_ward_by_id(id_ward)
    return JsonResponse({})


@require_POST
def add_ward(request):
    data = {'name': request.POST['ward_name']}
    if request.POST['id_ward'] == "":
        data["id"] = str(WardController().insert_ward(data))
        del data["_id"]
    else:
        WardController().update_ward(request.POST['id_ward'], request.POST['ward_name'])
        data['id'] = request.POST['id_ward']
    return JsonResponse(data)


@require_POST
def load_sub_ward(request):
    id_ward = request.POST['id_ward']
    ward = WardController().find_ward_by_id(id_ward)
    if ward:
        ward['id'] = str(ward.pop('_id'))
    return JsonResponse(ward)


@require_POST
def add_sub_ward(request):
    id_ward = request.POST['id_ward']
    vta_status = settings.DICT_BOOL[request.POST["vta_status"]]

    data = {
        'name': request.POST['sub_name'],
        'price': int(request.POST['sub_price']),
        'vta_status': vta_status,
    }
    if request.POST['id_sub_ward'] == "":
        WardController().insert_sub_ward(id_ward, data)
    else:
        data['id'] = int(request.POST['id_sub_ward'])
        WardController().update_sub_ward(id_ward, data)
    return JsonResponse(data)


@require_POST
def delete_sub_ward(request):
    id_ward = request.POST['id_ward']
    id_sub = request.POST['id_sub']
    WardController().delete_sub_ward(id_ward, id_sub)
    return JsonResponse({})

from reporting.views import date_reporting, reporting_visit, reporting_medicine
from django.conf.urls import url

urlpatterns = [
    url(r'^date_reporting/$', date_reporting, name='date_reporting'),
    url(r'^reporting_visit/$', reporting_visit, name='reporting_visit'),
    url(r'^reporting_medicine/$', reporting_medicine, name='reporting_medicine'),

]


# coding: utf-8
from django.http import HttpResponse
from nurse_driver.controller import NurseDriverCalendarController, NurseDriverController
from finance.controller import FinanceVisitController, FinanceMedicineController
from django.shortcuts import render, redirect
from collections import defaultdict
from core import psword
from core.static import START_DATE, END_DATE, EXPORT_TYPE, EXCEL,\
    CREATED_DATE, LIST_WARD, TOTAL_AMOUNT, ID_SUB_WARD, ID_WARD,\
    WARD_NAME, SUB_WARD_NAME, FINAL_REPORT, WARD_REPORTS, NURSE, DRIVER,\
    F_NAME, L_NAME, TYPE, LIST_MEDICINE, COUNT, EN_NAME, ID

import datetime
import jdatetime
import StringIO
import xlsxwriter


def date_reporting(request):
    return render(request, 'reporting/date_reporting.html')


def reporting_visit(request):
    start_date = request.GET.get(START_DATE)
    end_date = request.GET.get(END_DATE)
    export_type = request.GET.get(EXPORT_TYPE)
    if start_date and end_date:
        try:
            gregorian_start_date = convert_date_jalaji_to_gregorian(start_date)
            gregorian_end_date = convert_date_jalaji_to_gregorian(end_date)
            final_report, ward_reports = get_finance_ward_visit(gregorian_start_date, gregorian_end_date)

            if export_type == EXCEL and final_report:
                return visit_reporting_excel(final_report, ward_reports, start_date, end_date)
            else:
                return render(request, 'reporting/chart_reporting.html', {
                    FINAL_REPORT: final_report,
                    WARD_REPORTS: ward_reports
                })
        except:
            return redirect(date_reporting)
    else:
        return redirect(date_reporting)


def convert_date_jalaji_to_gregorian(start_date):
    start_date = start_date.split('-')
    start_date = jdatetime.JalaliToGregorian(int(start_date[0]), int(start_date[1]), int(start_date[2]))
    return datetime.datetime(start_date.gyear, start_date.gmonth, start_date.gday)


def get_finance_ward_visit(start_date, end_date):
    projection = {
        LIST_WARD: 1,
        TOTAL_AMOUNT: 1,
    }
    criteria = {CREATED_DATE: {
        '$gte': start_date,
        '$lt': end_date + datetime.timedelta(days=1)
    }}

    finances = FinanceVisitController().find_finances(criteria, projection)
    final_report, wards_report = process_finance_visit(finances)
    return dict(final_report), dict(wards_report)


def process_finance_visit(finances):
    final_reporting = defaultdict(lambda: {})
    wards_reporting = defaultdict(lambda: {})
    list_fake = []
    for finance in finances:
        for sub_ward in finance[LIST_WARD]:
            if sub_ward[ID_SUB_WARD] in list_fake:
                final_reporting[sub_ward[ID_WARD]][sub_ward[ID_SUB_WARD]][TOTAL_AMOUNT] += sub_ward[TOTAL_AMOUNT]
                wards_reporting[sub_ward[ID_WARD]][TOTAL_AMOUNT] += sub_ward[TOTAL_AMOUNT]
            else:
                list_fake.append(sub_ward[ID_SUB_WARD])

                final_reporting[sub_ward[ID_WARD]][WARD_NAME] = sub_ward[WARD_NAME]
                final_reporting[sub_ward[ID_WARD]][sub_ward[ID_SUB_WARD]] = {
                    ID_SUB_WARD: sub_ward[ID_SUB_WARD],
                    TOTAL_AMOUNT: sub_ward[TOTAL_AMOUNT],
                    SUB_WARD_NAME: sub_ward[SUB_WARD_NAME]
                }

                wards_reporting[sub_ward[ID_WARD]] = {
                    WARD_NAME: sub_ward[WARD_NAME],
                    TOTAL_AMOUNT: sub_ward[TOTAL_AMOUNT],
                }

    return final_reporting, wards_reporting


def visit_reporting_excel(finances, wards, start_date, end_date):
    header, output, response, text_style, title, title_2, total_style, workbook, worksheet_s = report_data("visit")
    title_text = psword.TITLE_REPORT_VISIT
    worksheet_s.merge_range('A2:D2', title_text.decode("utf-8"), title)
    worksheet_s.write(3, 0, psword.START_DATE.decode("utf-8"), header)
    worksheet_s.write(4, 0, start_date, text_style)
    worksheet_s.write(3, 2, psword.END_DATE.decode("utf-8"), header)
    worksheet_s.write(4, 2, end_date, text_style)
    if start_date == end_date:
        find_calendar = NurseDriverCalendarController().find_one(start_date)
        _dict = {NURSE: psword.NURSE, DRIVER: psword.DRIVER}
        if find_calendar:
            result = NurseDriverController().find([find_calendar[NURSE], find_calendar[DRIVER]])
            col = 4
            for item in result:
                full_name = item[F_NAME].encode("utf-8") + " " + item[L_NAME].encode("utf-8")
                worksheet_s.write(3, col, _dict[item[TYPE]].decode("utf-8"), header)
                worksheet_s.write(4, col, full_name.decode("utf-8"), text_style)
                col += 1

    worksheet_s.merge_range('A7:D7', psword.REPORTS.decode("utf-8"), title_2)
    worksheet_s.write(8, 0, psword.INCOME.decode("utf-8"), header)
    worksheet_s.write(8, 1, psword.WARD_NAME.decode("utf-8"), header)
    row = 9
    total_amount = 0
    for key, ward in wards.items():
        total_amount += ward[TOTAL_AMOUNT]
        worksheet_s.write(row, 0, ward[TOTAL_AMOUNT], text_style)
        worksheet_s.write(row, 1, ward[WARD_NAME].encode("utf-8").decode("utf-8"), text_style)
        row += 1

    worksheet_s.write(row, 1, psword.TOTAL_AMOUNT.decode("utf-8"), header)
    worksheet_s.write(row, 0, total_amount, total_style)

    row += 3
    worksheet_s.merge_range('A{0}:D{0}'.format(row), psword.TITLE_SUB_WARD.decode("utf-8"), title_2)
    row += 1
    for key, finance in finances.items():
        total_amount = 0
        ward_name = ""
        worksheet_s.write(row, 0, psword.INCOME.decode("utf-8"), header)
        worksheet_s.write(row, 1, psword.SUB_WARD_NAME.decode("utf-8"), header)
        row += 1
        for key1, sub in finance.items():
            if isinstance(sub, dict):
                total_amount += sub[TOTAL_AMOUNT]
                worksheet_s.write(row, 0, sub[TOTAL_AMOUNT], text_style)
                worksheet_s.write(row, 1, sub[SUB_WARD_NAME].encode("utf-8").decode("utf-8"), text_style)
                row += 1
            else:
                ward_name = sub

        worksheet_s.write(row, 0, total_amount, total_style)
        worksheet_s.write(row, 1, psword.TOTAL_AMOUNT.decode("utf-8"), header)
        worksheet_s.write(row, 2, ward_name.encode("utf-8").decode("utf-8"), header)
        row += 4

    workbook.close()
    xlsx_data = output.getvalue()

    response.write(xlsx_data)
    return response


def reporting_medicine(request):
    start_date = request.GET.get(START_DATE)
    end_date = request.GET.get(END_DATE)
    if start_date and end_date:
        try:
            gregorian_start_date = convert_date_jalaji_to_gregorian(start_date)
            gregorian_end_date = convert_date_jalaji_to_gregorian(end_date)
            finances = get_finance_medicine(gregorian_start_date, gregorian_end_date)
            return medicine_reporting_excel(finances, start_date, end_date)
        except:
            return redirect(date_reporting)
    return


def get_finance_medicine(start_date, end_date):
    projection = {
        LIST_MEDICINE: 1,
        TOTAL_AMOUNT: 1,
    }
    criteria = {CREATED_DATE: {
        '$gte': start_date,
        '$lt': end_date + datetime.timedelta(days=1)
    }}

    finances = FinanceMedicineController().find_finances(criteria, projection)
    final_reporting = process_finance_medicine(finances)
    return final_reporting


def process_finance_medicine(finances):
    final_reporting = defaultdict(lambda: {})
    list_fake = []
    for finance in finances:
        for medicine in finance[LIST_MEDICINE]:
            if medicine[ID] in list_fake:
                final_reporting[medicine[ID]][TOTAL_AMOUNT] += medicine[TOTAL_AMOUNT]
                final_reporting[medicine[ID]][COUNT] += medicine[COUNT]
            else:
                list_fake.append(medicine[ID])
                final_reporting[medicine[ID]] = {
                    TOTAL_AMOUNT: medicine[TOTAL_AMOUNT],
                    COUNT: medicine[COUNT],
                    EN_NAME: medicine[EN_NAME]
                }
    return final_reporting


def medicine_reporting_excel(reports, start_date, end_date):
    header, output, response, text_style, title, title_2, total_style, workbook, worksheet_s = report_data("medicine")
    title_text = psword.TITLE_REPORT_MEDICINE
    worksheet_s.merge_range('A2:D2', title_text.decode("utf-8"), title)
    worksheet_s.write(3, 0, psword.START_DATE.decode("utf-8"), header)
    worksheet_s.write(4, 0, start_date, text_style)
    worksheet_s.write(3, 2, psword.END_DATE.decode("utf-8"), header)
    worksheet_s.write(4, 2, end_date, text_style)

    worksheet_s.merge_range('A7:D7', psword.REPORTS.decode("utf-8"), title_2)
    worksheet_s.write(8, 2, psword.NAME_MEDICINE.decode("utf-8"), header)
    worksheet_s.write(8, 1, psword.COUNT.decode("utf-8"), header)
    worksheet_s.write(8, 0, psword.TOTAL_AMOUNT.decode("utf-8"), header)

    row = 9
    total_amount = 0
    for key, report in reports.items():
        total_amount += report[TOTAL_AMOUNT]
        worksheet_s.write(row, 0, report[TOTAL_AMOUNT], text_style)
        worksheet_s.write(row, 1, report[COUNT], text_style)
        worksheet_s.write(row, 2, report[EN_NAME].encode("utf-8").decode("utf-8"), text_style)
        row += 1

    worksheet_s.write(row, 1, psword.TOTAL_AMOUNT.decode("utf-8"), header)
    worksheet_s.write(row, 0, total_amount, total_style)

    workbook.close()
    xlsx_data = output.getvalue()

    response.write(xlsx_data)
    return response


def report_data(report_type):
    response = HttpResponse(content_type='text/xlsx')
    response['Content-Disposition'] = 'attachment; filename="{0}_report.xlsx"'.format(report_type)
    output = StringIO.StringIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet_s = workbook.add_worksheet(report_type)
    title = workbook.add_format({
        'bold': True,
        'font_size': 14,
        'align': 'center',
        'color': 'red',
        'valign': 'vcenter'
    })
    title_2 = workbook.add_format({
        'bold': True,
        'font_size': 14,
        'align': 'center',
        'color': 'green',
        'valign': 'vcenter'
    })
    header = workbook.add_format({
        'bg_color': '#F7F7F7',
        'color': 'black',
        'align': 'center',
        'valign': 'top',
        'border': 1
    })
    text_style = workbook.add_format({
        'color': 'black',
        'align': 'center',
        'valign': 'top',
        'text_wrap': True,
        'font_size': 10
    })
    total_style = workbook.add_format({
        'color': 'red',
        'align': 'center',
        'valign': 'top',
        'text_wrap': True,
        'border': 1
    })
    return header, output, response, text_style, title, title_2, total_style, workbook, worksheet_s

from patients.views import patients, delete_patient, add_patient, edit_patient
from django.conf.urls import url

urlpatterns = [
    url(r'^patients/$', patients, name='patients'),
    url(r'^patient/delete/$', delete_patient, name='delete_patient'),
    url(r'^patient/add/$', add_patient, name='add_patient'),
    url(r'^patient/edit/$', edit_patient, name='edit_patient'),
]


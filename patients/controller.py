import datetime
import re
from core import cursor
from bson.objectid import ObjectId
from core.static import CREATED_DATE, PATIENT_NAME, NATIONAL_CODE, FULL_NAME,\
    L_NAME, F_NAME, ID_


class PatientsController(object):
    def __init__(self):
        self.db = cursor.patients

    def get_all_patients(self):
        return self.db.find().sort(CREATED_DATE, -1)

    def insert_patient(self, data):
        data[CREATED_DATE] = datetime.datetime.now()
        return self.db.insert(data)

    def delete_patient_by_id(self, id_patient):
        self.db.remove(
            {ID_: ObjectId(id_patient)}
        )

    def find_patient_by_patient_name(self, patient_name):
        return self.db.find_one({PATIENT_NAME: patient_name})

    def find_patient_by_id(self, patient_id):
        return self.db.find_one({ID_: ObjectId(patient_id)})

    def find_patient_by_national_code(self, national_code):
        return self.db.find_one({NATIONAL_CODE: national_code})

    def update_patient(self, data, id_patient):
        return self.db.update_one(
            {ID_: ObjectId(id_patient)},
            {
                '$set': {key: value for key, value in data.items()}
            }
        )

    def find_patient_by_regex(self, word):
        regex = re.compile('.*' + word + '.*', re.IGNORECASE)
        criteria = {FULL_NAME: {'$regex': regex}}
        projection = {
            F_NAME: 1,
            L_NAME: 1,
            FULL_NAME: 1,
            NATIONAL_CODE: 1,
        }
        return self.db.find(criteria, projection)

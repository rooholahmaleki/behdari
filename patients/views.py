#! -*- coding: utf-8 -*-
# coding: utf-8

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET
from controller import PatientsController
from core.static import PATIENTS, ID_PATIENT, NATIONAL_CODE, MESSAGE,\
    ID, FULL_NAME, L_NAME, F_NAME, ID_, DATA


@require_GET
def patients(request):
    args = {
        PATIENTS: PatientsController().get_all_patients()
    }
    return render(request, 'patient/patients.html', args)


@require_POST
def delete_patient(request):
    id_patient = request.POST[ID_PATIENT]
    PatientsController().delete_patient_by_id(id_patient)
    return JsonResponse({})


@require_POST
def add_patient(request):
    find_patient = False
    if request.POST[NATIONAL_CODE] != "0":
        find_patient = PatientsController().find_patient_by_national_code(request.POST[NATIONAL_CODE])

    if not find_patient:
        data = {
            F_NAME: request.POST[F_NAME],
            L_NAME: request.POST[L_NAME],
            FULL_NAME: request.POST[F_NAME]+" "+request.POST[L_NAME],
            NATIONAL_CODE: request.POST[NATIONAL_CODE]
        }
        if request.POST[ID_PATIENT] != "":
            PatientsController().update_patient(data, request.POST[ID_PATIENT])
            data[ID] = request.POST[ID_PATIENT]
        else:
            PatientsController().insert_patient(data)
            data[ID] = str(data.pop(ID_))
        data[MESSAGE] = 'ok'
    else:
        data = {
            MESSAGE: 'repeat_national_code'
        }
    return JsonResponse({DATA: data})


@require_POST
def edit_patient(request):
    id_patient = request.POST[ID_PATIENT]
    patient = PatientsController().find_patient_by_id(id_patient)
    if patient:
        patient[ID] = str(patient.pop(ID_))
    return JsonResponse(patient)

from core import cursor
from bson.objectid import ObjectId
from core.static import CREATED_DATE, ID_, COUNT, EN_NAME, FA_NAME, PRICE
import datetime
import re


class MedicineController(object):
    def __init__(self):
        self.db = cursor.medicine

    def get_all_medicine(self):
        return self.db.find().sort(CREATED_DATE, -1)

    def insert_medicine(self, data):
        data[CREATED_DATE] = datetime.datetime.now()
        return self.db.insert(data)

    def update_medicine(self, data, id_medicine):
        data["modified_date"] = datetime.datetime.now()
        return self.db.update_one(
            {ID_: ObjectId(id_medicine)},
            {
                '$set': {key: value for key, value in data.items()}
            }
        ).matched_count

    def decrease_medicine_count(self, id_medicine, count):
        count = int(self.find_medicine_by_id(id_medicine)[COUNT]) - count
        return self.update_medicine({COUNT: count}, id_medicine)

    def increase_medicine_count(self, id_medicine, count):
        count += int(self.find_medicine_by_id(id_medicine)[COUNT])
        medicine = self.update_medicine({COUNT: count}, id_medicine)
        if medicine:
            return count
        return False

    def delete_medicine_by_id(self, id_medicine):
        self.db.remove({ID_: ObjectId(id_medicine)})

    def find_medicine_by_id(self, id_medicine):
        return self.db.find_one({ID_: ObjectId(id_medicine)})

    def find_medicine_by_regex(self, word):
        regex = re.compile('.*' + word + '.*', re.IGNORECASE)
        criteria = {"$or": [{EN_NAME: {'$regex': regex}}, {FA_NAME: {'$regex': regex}}]}
        projection = {
            EN_NAME: 1,
            FA_NAME: 1,
            COUNT: 1,
            PRICE: 1,
        }
        return self.db.find(criteria, projection)

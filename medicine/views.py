#! -*- coding: utf-8 -*-
# coding: utf-8
from django.shortcuts import render
from django.http import JsonResponse
from controller import MedicineController
from core.templatetags import general_template
from django.views.decorators.http import require_POST
from core.static import MEDICINES, ID_, COUNT, EN_NAME, FA_NAME, PRICE, START_DATE, \
    END_DATE, ID, ID_MEDICINE, SUCCESS, MESSAGE, DICT_DATE, COUNT_PACKAGE, \
    PERSIAN_DATE, DATE_TYPE, HELICAL, ERROR, MEDICINE_COUNT

import datetime
import jdatetime


def medicines(request):
    args = {
        MEDICINES: MedicineController().get_all_medicine()
    }
    return render(request, 'medicine.html', args)


@require_POST
def add_medicine(request):
    dict_result = {
        FA_NAME: request.POST[FA_NAME],
        EN_NAME: request.POST[EN_NAME],
        PRICE: int(request.POST[PRICE])
    }
    dict_date = {
        START_DATE: request.POST[START_DATE].strip(),
        END_DATE: request.POST[END_DATE].strip(),
    }

    if request.POST[DATE_TYPE] == HELICAL:
        dict_result[DICT_DATE] = convert_date_jalaji_to_gregorian(dict_date)
        dict_result[DICT_DATE][PERSIAN_DATE] = True

    else:
        dict_result[DICT_DATE] = return_date_gregorian(dict_date)
        dict_result[DICT_DATE][PERSIAN_DATE] = False

    if request.POST[ID_MEDICINE] != "":
        MedicineController().update_medicine(dict_result, request.POST[ID_MEDICINE])
        id_medicine = request.POST[ID_MEDICINE]

    else:
        count = request.POST.get(COUNT)
        count_package = request.POST.get(COUNT_PACKAGE)
        if count and count.isdigit():
            dict_result[COUNT] = int(count)
        else:
            dict_result[COUNT] = 0
        if count_package and count_package.isdigit():
            count_package = int(request.POST[COUNT_PACKAGE])
            dict_result[COUNT] = count * count_package

        id_medicine = MedicineController().insert_medicine(dict_result)

    dict_result[MESSAGE] = SUCCESS
    dict_result[ID] = str(dict_result.pop("_id")) if "_id" in dict_result else id_medicine
    dict_result[DICT_DATE][START_DATE] = general_template.persian_date(dict_result[DICT_DATE][START_DATE])
    dict_result[DICT_DATE][END_DATE] = general_template.persian_date(dict_result[DICT_DATE][END_DATE])
    return JsonResponse(dict_result)


@require_POST
def add_count_medicine(request):
    medicine_count = 0
    id_medicine = request.POST[ID_MEDICINE]
    count = request.POST.get(COUNT, 0)

    message = ERROR
    if count and count.isdigit():
        if request.POST.get(COUNT_PACKAGE) and request.POST[COUNT_PACKAGE].isdigit():
            count = int(request.POST[COUNT]) * int(request.POST[COUNT_PACKAGE])

        medicine_count = MedicineController().increase_medicine_count(id_medicine, int(count))
        if medicine_count:
            message = SUCCESS

    return JsonResponse({MESSAGE: message, MEDICINE_COUNT: medicine_count})


def check_data_is_valid(dict_date):
    bool = True
    for item in dict_date:
        if dict_date[item] < 0:
            bool = False
            break
    return bool


def convert_date_jalaji_to_gregorian(dict_date):
    start_date = dict_date[START_DATE].split('/')
    end_date = dict_date[END_DATE].split('/')
    start_date = jdatetime.JalaliToGregorian(int(start_date[0]), int(start_date[1]), int(start_date[2]))
    end_date = jdatetime.JalaliToGregorian(int(end_date[0]), int(end_date[1]), int(end_date[2]))
    dict_date[START_DATE] = datetime.datetime(start_date.gyear, start_date.gmonth, start_date.gday)
    dict_date[END_DATE] = datetime.datetime(end_date.gyear, end_date.gmonth, end_date.gday)
    return dict_date


def return_date_gregorian(dict_date):
    dict_date[START_DATE] = datetime.datetime.strptime(dict_date[START_DATE], '%Y/%m/%d')
    dict_date[END_DATE] = datetime.datetime.strptime(dict_date[END_DATE], '%Y/%m/%d')
    return dict_date


@require_POST
def delete_medicine(request):
    id_medicine = request.POST[ID_MEDICINE]
    MedicineController().delete_medicine_by_id(id_medicine)
    return JsonResponse({})


@require_POST
def edit_medicine(request):
    id_medicine = request.POST[ID_MEDICINE]
    medicine = MedicineController().find_medicine_by_id(id_medicine)
    if medicine:
        medicine[ID] = str(medicine.pop(ID_))
        return_convert_date(medicine)
    return JsonResponse(medicine)


def return_convert_date(medicine):
    if medicine[DICT_DATE][PERSIAN_DATE]:
        medicine[DICT_DATE][START_DATE] = general_template.persian_date(medicine[DICT_DATE][START_DATE])
        medicine[DICT_DATE][END_DATE] = general_template.persian_date(medicine[DICT_DATE][END_DATE])
    else:
        medicine[DICT_DATE][START_DATE] = str(medicine[DICT_DATE][START_DATE]).replace('-', '/').split(' ')[0]
        medicine[DICT_DATE][END_DATE] = str(medicine[DICT_DATE][END_DATE]).replace('-', '/').split(' ')[0]

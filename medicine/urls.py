from medicine.views import medicines, add_medicine, delete_medicine, edit_medicine, add_count_medicine
from django.conf.urls import url


urlpatterns = [
    url(r'^medicines/$', medicines, name='medicines'),
    url(r'^medicines/add/$', add_medicine, name='add_medicine'),
    url(r'^medicines/add_count/$', add_count_medicine, name='add_count_medicine'),
    url(r'^medicines/delete/$', delete_medicine, name='delete_medicine'),
    url(r'^medicines/edit/$', edit_medicine, name='edit_medicine'), #load data to modal
]


from django.shortcuts import render, redirect
from django.contrib import auth
from core.static import MESSAGE, POST, USER_NAME, PASSWORD


def login(request):
    args = {MESSAGE: False}
    if request.method == POST:
        user = auth.authenticate(
            username=request.POST[USER_NAME],
            password=request.POST[PASSWORD]
        )
        if user:
            auth.login(request, user)
            return redirect(home)
        else:
            args = {MESSAGE: True}
            return render(request, 'login.html', args)
    else:
        return render(request, 'login.html', args)


def home(request):
    return render(request, 'home.html')


def logout(request):
    auth.logout(request)
    return redirect(login)

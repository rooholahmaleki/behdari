#! -*- coding: utf-8 -*-
# coding: utf-8

from __future__ import unicode_literals
from django import template
from core import cursor
from bson.json_util import ObjectId

import jdatetime

register = template.Library()


@register.filter(name='mongo_id')
def mongo_id(value):
    return str(value['_id'])


@register.filter(name='persian_date')
def persian_date(date):
    return_date = jdatetime.date.fromgregorian(
        day=date.day,
        month=date.month,
        year=date.year,
    )
    if date.hour == 0 and date.minute == 0:
        hour = ""
    else:
        hour = " - " + str(date.hour) + ":" + str(date.minute)

    return str(return_date).replace('-', '/').split(' ')[0] + hour


@register.filter(name='commas_int')
def commas_int(number):
    if isinstance(number, int) and number != "0":
        return "{:,}".format(int(number))
    return 0


@register.filter(name='total_amount_visit')
def total_amount_visit(finance_visit):
    price = finance_visit["sum_vta_price"] + finance_visit["sum_price"]
    return "{:,}".format(int(price))


@register.filter
def has_perm(user, perm):
    if not hasattr(user, 'userBox'):
        return False
    else:
        return cursor.acl.find_one(
            {'_id': ObjectId(user.userBox.user_type), 'visible_templatetags': str(perm)},
            {'visible_templatetags': 1, '_id': 0}
        )


@register.filter
def has_perm_by_list(user, list_perm):
    list_perm = list_perm.split(',')
    if not hasattr(user, 'userBox'):
        return False
    else:
        return cursor.acl.find_one(
            {'_id': ObjectId(user.userBox.user_type), 'visible_templatetags': {"$in": list_perm}},
            {'visible_templatetags': 1, '_id': 0}
        )


@register.filter(name='get_bool')
def get_bool(bool):
    if bool:
        return "دارد"
    return "ندارد"

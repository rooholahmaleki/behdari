import hashlib
from users.models import UserBox
from django.contrib.auth.models import User
from users.controller import UsersController
from django.core.exceptions import ObjectDoesNotExist
from core.static import PASSWORD, USER_NAME, ID_, L_NAME, F_NAME, USER_TYPE


class ClinicBackend(object):
    def authenticate(self, username, password):
        user = return_user(username, password)
        if user:
            delete_old_user(user)
            user_model = create_new_user(user)
            return user_model
        else:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


def return_user(username, password):
    user = UsersController().find_user_by_user_name_and_status(username)
    if user:
        if check_password(user[PASSWORD], password):
            return user
        else:
            return None
    else:
        return None


def delete_old_user(user):
    try:
        UserBox.objects.get(mongo_id=str(user[ID_])).delete()
    except ObjectDoesNotExist:
        pass

    try:
        User.objects.get(username=user[USER_NAME]).delete()
    except ObjectDoesNotExist:
        pass


def create_new_user(user):
    user_model = User(username=user[USER_NAME], password=user[PASSWORD])
    user_model.email = user[USER_NAME]
    user_model.save()

    UserBox.objects.create(
        user=user_model,
        mongo_id=str(user[ID_]),
        full_name=user[F_NAME] + " " + user[L_NAME],
        user_type=user[USER_TYPE],
    )
    return user_model


def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()

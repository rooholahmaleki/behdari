# coding: utf-8

EN_NAME = 'نام انگلیسی'
NURSE = 'بهیار'
DRIVER = 'راننده'
F_NAME = 'نام'
L_NAME = 'نام خانوادگی'
PHONE = 'تلفن'

USER_TYPE = 'نوع کاربر'
GROUP_MESSAGE = 'نام گروه را وارد کنید.'
F_NAME_MESSAGE = 'نام را وارد کنید.'
L_NAME_MESSAGE = 'نام خانوادگی را وارد کنید.'
PHONE_MESSAGE = 'شماره تلفن را وارد کنید.'
USER_TYPE_MESSAGE = 'نوع کاربر را وارد کنید.'
TITLE_REPORT_VISIT = "گزارش بخش خدمات بهداری شهرک صنعتی عباس آباد"
TITLE_REPORT_MEDICINE = "گزارش بخش داروخانه بهداری شهرک صنعتی عباس آباد"
START_DATE = "تاریخ شروع"
END_DATE = "تاریخ پایان"
INSERT_ERROR = 'اضافه نشد.'
REPORTS = "گزارش کلی"
INCOME = "درآمد"
WARD_NAME = "نام بخش"
SUB_WARD_NAME = "نام زیر بخش"
TOTAL_AMOUNT = "جمع کل"
NAME_MEDICINE = "نام دارو"
COUNT = "تعداد"
TITLE_SUB_WARD = "گزارش به تفکیک بخش"

from db_mongo import MongoConnection

def MongoCursorDefs(db):
    return MongoConnection().getCursor(db)

cursor = MongoCursorDefs('Clinic')

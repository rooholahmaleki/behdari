from django.conf import settings
from pymongo import MongoClient


class MongoConnection(object):
    __db = None

    @classmethod
    def get_connection(cls):
        """
            Singleton method for running Mongo instance
        """
        if cls.__db is None:
            mongo_config = settings.MONGO
            cls.__db = MongoClient(mongo_config['HOST'], mongo_config['PORT'],
                                   maxPoolSize=None)
        return cls.__db

    def __init__(self):
        self.get_connection()

    # def get_cursor(self):
    #     return self.__db
    def getCursor(self, db):
        self.__db_cursor = self.__db[db]
        self.__db_cursor.authenticate(
            settings.MONGO['USER'],
            settings.MONGO['PASSWORD'],
            source='admin')

        return self.__db_cursor

from users.views import users, delete_user, add_user, edit_user
from django.conf.urls import url

urlpatterns = [
    url(r'^users/$', users, name='users'),
    url(r'^user/delete/$', delete_user, name='delete_user'),
    url(r'^user/add/$', add_user, name='add_user'),
    url(r'^user/edit/$', edit_user, name='edit_user'),
]


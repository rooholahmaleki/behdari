import re
import datetime
from core import cursor
from bson.objectid import ObjectId


class UsersController(object):
    def __init__(self):
        self.db = cursor.users

    def get_all_users(self):
        return self.db.find({"status": True}).sort('created_date', -1)

    def insert_user(self, data):
        data['created_date'] = datetime.datetime.now()
        return self.db.insert(data)

    def delete_user_by_id(self, id_user):
        self.db.update_one(
            {'_id': ObjectId(id_user)},
            {'$set': {"status": False}}
        )

    def find_user_by_user_name(self, user_name):
        return self.db.find_one({'user_name': user_name})

    def find_user_by_user_name_and_status(self, user_name):
        return self.db.find_one({'user_name': user_name, "status": True})

    def find_user_by_user_name_password(self, user_name, password):
        return self.db.find_one({'user_name': user_name, 'password': password})

    def find_user_by_id(self, user_id):
        return self.db.find_one({'_id': ObjectId(user_id)}, {'password': 0})

    def update_user(self, data, id_user):
        return self.db.update_one(
            {'_id': ObjectId(id_user)},
            {
                '$set': {key: value for key, value in data.items()}
            }
        )

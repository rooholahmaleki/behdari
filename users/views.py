#! -*- coding: utf-8 -*-
import uuid
import hashlib
from django.shortcuts import render
from django.http import JsonResponse
from controller import UsersController
from django.views.decorators.http import require_POST, require_GET
from acl.controller import AclController


@require_GET
def users(request):
    _users = list(UsersController().get_all_users())
    for user in _users:
        group = AclController().find_group_by_id(user["user_type"])
        if group:
            user["user_type"] = group.get("name", "تعریف نشده")
        else:
            user["user_type"] = "تعریف نشده"

    args = {
        'users': _users,
        'groups': AclController().get_all_group_name()
    }
    return render(request, 'users.html', args)


@require_POST
def delete_user(request):
    id_user = request.POST['id_user']
    UsersController().delete_user_by_id(id_user)
    return JsonResponse({})


@require_POST
def add_user(request):
    data = {}
    if request.POST['id_user'] == "":
        find_user = UsersController().find_user_by_user_name(request.POST['user_name'])
    else:
        find_user = False
    if not find_user:
        data = {
            'f_name': request.POST['f_name'],
            'l_name': request.POST['l_name'],
            'full_name': request.POST['f_name'] + " " + request.POST['l_name'],
            'user_name': request.POST['user_name'],
            'user_type': request.POST['user_type'],
            'status': True,
            'password': hash_password(request.POST['password'])
        }
        if request.POST['id_user'] != "":
            UsersController().update_user(data, request.POST['id_user'])
            data['id'] = request.POST['id_user']
        else:
            UsersController().insert_user(data)
            data['id'] = str(data.pop('_id'))
        del data['password']
        data["user_type"] = AclController().find_group_by_id(request.POST['user_type']).get("name", "تعریف نشده")
        message = "success"
    else:
        message = "repeat_user"
    return JsonResponse({'message': message, 'data': data})


def hash_password(password):
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ':' + salt


def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()


@require_POST
def edit_user(request):
    id_user = request.POST['id_user']
    user = UsersController().find_user_by_id(id_user)
    if user:
        user['id'] = str(user.pop('_id'))
    return JsonResponse(user)

var ComponentsTypeahead = function () {
    var e = function () {
        $("#name_medicine").attr("dir", "rtl"),
        $("#name_medicine").typeahead(null, {
            source: function (query) {
                return $.get('/finance/autocomplete/medicine/', { medicine: query }, function (data) {
                    for(var item in data){
                        var html = '<div class="tt-suggestion tt-selectable">'+data[item]['en_name']+'( '+data[item]['fa_name']+' )</div>';
                        $('.tt-menu').removeClass('tt-empty');
                        $('.tt-menu').css('display', 'block');
                        $('.tt-dataset').append(html);
                        $("#en_name_medicine_label").html('نام:'+' '+data[item]['en_name']+'('+data[item]['fa_name']+')');
                        $("#price_medicine_label").html('قیمت(ریال):'+' '+data[item]['price']);
                        $("#count_medicine_label").html('موجودی:'+' '+data[item]['count']);
                        $("#id_medicine").val(data[item]['id'])
                    }
                    if(data.length == 0){
                        var html = '<div class="tt-suggestion tt-selectable">چیزی یافت نشد</div>';
                        $('.tt-dataset').append(html);
                        $('.tt-menu').removeClass('tt-empty');
                        $('.tt-menu').css('display', 'block');
                        clean_input_html();

                    }
                });
            }
        });
    };
    return {
        init: function () {
            e()
        }
    }
}();
jQuery(document).ready(function () {
    ComponentsTypeahead.init()
});

function clean_input_html(){
    $("#en_name_medicine_label").html('نام  :');
    $("#price_medicine_label").html('قیمت(ریال)  :0');
    $("#count_medicine_label").html('موجودی:0');
    $("#id_medicine").val('');
    $("#count_medicine").val('');
}
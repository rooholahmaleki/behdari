var csrf_token;
var count = 1;
var id_patient = 'guest';

function add_finance() {
    var received_price = $('#received_price').val();
    var patient_name = $('#patient_name').val();
    if (received_price != "" && check_type_user(patient_name)) {
        if ($("#id_patient").val() == "") {
            id_patient = 'guest';
        } else {
            id_patient = $("#id_patient").val();
        }
        var data = {
            'csrfmiddlewaretoken': csrf_token,
            'received_price': received_price,
            'patient_name': patient_name,
            'id_patient': id_patient
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/finance/add/finance/medicine/',
            data: data,
            success: function (resp) {
                if (resp['message'] == "success") {
                    window.open(
                        '/finance/print/finance/medicine/' + resp['id_medicine'],
                        '_blank'
                    );
                    location.reload();

                } else if (resp['message'] == "error"){
                    uikitNotify('هیچ دارویی در فاکتور نیست', '', 'danger');

                }else {
                    uikitNotify('هیچ دارویی در فاکتور نیست', '', 'danger');
                }

            },
            error: function (e) {
                notify_error(e)
            }
        });
    } else {
        if (received_price == "") {
            uikitNotify('مبلغ دریافتی را وارد کنید.', '', 'danger');
        }
    }
}

function check_type_user(patient_name) {
    var bool = true;
    if (patient_name == "") {
        bool = false;
        uikitNotify('خریدار مشخص نیست', '', 'danger');
    }
    return bool
}

function add_medicine() {
    var count_medicine = $("#count_medicine").val();
    var id_medicine = $("#id_medicine").val();
    if (id_medicine != "" && count_medicine != "") {
        var data = {
            'count_medicine': count_medicine,
            'id_medicine': id_medicine,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/finance/add/medicine/',
            data: data,
            success: function (resp) {
                if (resp['message'] == "success") {
                    var price_count = (parseInt(resp['price']) * parseInt(count_medicine));
                    var sum_price = parseInt($("#sum_price_val").val()) + price_count;
                    $("#sum-price").html(numberWithCommas(sum_price));
                    $("#sum_price_val").val(sum_price);
                    $("#received_price").val(sum_price);
                    var medicine_html =
                        '<tr id="' + count + "_" + resp['id'] + '" class="medicine_finance">' +
                        '<td class="count_number"></td>' +
                        '<td>' + resp['en_name'] + '</td>' +
                        '<td>' + count_medicine + '</td>' +
                        '<td>' +
                        '<a onclick="delete_medicine(\'' + id_medicine + '\', \'' + count_medicine + '\', \'' + count + '\')" class="btn btn-circle btn-icon-only btn-default btn red">' +
                        '<i class="icon-trash"></i>' +
                        '</a>' +
                        '</td>' +
                        '<td>' + price_count + '</td>' +
                        '</tr>';
                    $("#all_medicine > tr:first ").before(medicine_html);
                    clean_input_html();
                    $("#name_medicine").val('');
                    set_count_number();
                } else {
                    uikitNotify('مقدار فروش بیشتر از موجودی در انبار می باشد.', '', 'danger');
                }
            },
            error: function (e) {
                notify_error(e)
            }
        });
    } else {
        uikitNotify('دارو انتخاب نشده یا تعداد مشخص نیست.', '', 'danger');
    }
}
function delete_medicine(id_medicine, count_medicine, count_id) {
    var data = {
        'count_medicine': count_medicine,
        'id_medicine': id_medicine,
        'csrfmiddlewaretoken': csrf_token
    };
    $.ajax({
        dataType: 'json',
        type: 'POST',
        async: false,
        url: '/finance/delete/medicine/',
        data: data,
        success: function (resp) {
            $('#' + count_id + "_" + id_medicine).remove();
            var price_count = (parseInt(resp['price']) * parseInt(count_medicine));
            var sum_price = parseInt($("#sum_price_val").val()) - price_count;
            $("#sum-price").html(numberWithCommas(sum_price));
            $("#sum_price_val").val(sum_price);
            $("#received_price").val(sum_price);
            set_count_number();
        },
        error: function (e) {
            notify_error(e)
        }
    });
}
function clear_table() {
    $('.medicine_finance').each(function () {
        $(this).remove();
    });
    $("#sum-price").html(numberWithCommas(0));
    $("#sum_price_val").val(0);
    $("#received_price").val(0);
}
function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function set_count_number() {
    var count_number = 1;
    $('.count_number').each(function () {
        $(this).html(count_number);
        count_number += 1;
    });
}

$(".number").on("keypress", function (event) {
    var englishAlphabetAndWhiteSpace = /^([0-9])$/;
    var key = String.fromCharCode(event.which);
    if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
        return true;
    }
    uikitNotify('عدد انگلیسی وارد کنید.', '', 'danger');
    return false;
});
$('.number').on("paste", function (e) {
    e.preventDefault();
});

var ChartsAmcharts = function () {
    var Charts_M = function () {
        var e = AmCharts.makeChart("chart_medicine_finance", {
            theme: "light",
            type: "serial",
            startDuration: 2,
            fontFamily: "yekan",
            color: "#888",
            dataProvider: [
                {country: "", visits: 0, color: "#FFF"},
                {country: "اسفند", visits: 2500, color: "#FF0F00"},
                {country: "بهمن", visits: 1882, color: "#FF6600"},
                {country: "دی", visits: 1809, color: "#FF9E01"},
                {country: "اذر", visits: 1322, color: "#FCD202"},
                {country: "ابان", visits: 1122, color: "#F8FF01"},
                {country: "مهر", visits: 1114, color: "#B0DE09"},
                {country: "شهریور", visits: 984, color: "#04D215"},
                {country: "مرداد", visits: 711, color: "#0D8ECF"},
                {country: "تیر", visits: 665, color: "#0D52D1"},
                {country: "خرداد", visits: 580, color: "#2A0CD0"},
                {country: "اردیبهشت", visits: 443, color: "#8A0CCF"},
                {country: "فروردین", visits: 441, color: "#CD0D74"}
            ],
            valueAxes: [{position: "left", axisAlpha: 0, gridAlpha: 1}],
            graphs: [{
                balloonText: "[[category]]: <b>[[value]]</b>",
                colorField: "color",
                fillAlphas: 1,
                lineAlpha: .1,
                type: "column",
                topRadius: 1,
                valueField: "visits"
            }],
            depth3D: 40,
            angle: 30,
            chartCursor: {
                categoryBalloonEnabled: !1,
                cursorAlpha: 0,
                zoomable: !1
            },
            categoryField: "country",
            categoryAxis: {
                gridPosition: "start",
                axisAlpha: 0,
                gridAlpha: 0
            }
        }, 0);
    };
    return {
        init: function () {
            Charts_M()
        }
    }
}();
jQuery(document).ready(function () {
    ChartsAmcharts.init()
});
var head_patient = function () {
    var e = function () {
        $("#patient_name").attr("dir", "rtl"),
        $("#patient_name").typeahead(null, {
            source: function (query) {
                return $.get('/finance/autocomplete/patient/', { patient: query }, function (data) {
                    for(var item in data){
                        var html = '<div class="tt-suggestion tt-selectable"' +
                            ' onclick="load_data_patient(\''+data[item]['f_name']+'\', \''+data[item]['l_name']+'\', \''+data[item]['national_code']+'\', \''+data[item]['id']+'\')">' +
                            ''+data[item]['full_name']+
                            '</div>';
                        $('#patient_input .tt-menu').removeClass('tt-empty');
                        $('#patient_input .tt-menu').css('display', 'block');
                        $('#patient_input .tt-dataset').append(html);
                        $("#f_name").html('نام:'+' '+data[item]['f_name']);
                        $("#l_name").html('نام خانوادگی:'+' '+data[item]['l_name']);
                        $("#national_code").html('شماره ملی:'+' '+data[item]['national_code']);
                        $("#id_patient").val(data[item]['id'])
                    }
                    if(data.length == 0){
                        var html = '<div class="tt-suggestion tt-selectable">چیزی یافت نشد</div>';
                        $('#patient_input .tt-dataset').append(html);
                        $('#patient_input .tt-menu').removeClass('tt-empty');
                        $('#patient_input .tt-menu').css('display', 'block');
                        clean_input_patient_html();
                    }
                });
            }
        });
    };
    return {
        init: function () {
            e()
        }
    }
}();

function clean_input_patient_html(){
    $("#f_name").html('نام  : مهمان');
    $("#l_name").html('نام خانوادگی  : مهمان');
    $("#national_code").html('شماره ملی: 0');
    $("#id_patient").val('');
}

function change_display_patient_html(){
    if($('#check_type_patient:checked').length == 0){
        $("#f_name").html('نام: مهمان');
        $("#l_name").html('نام خانوادگی: مهمان');
        $("#patient_name").val('مهمان');
        $("#national_code").html('');
    }else{
        $("#f_name").html('نام:');
        $("#l_name").html('نام خانوادگی:');
        $("#national_code").html('شماره ملی:');
        $("#patient_name").val('');
    }
    $("#id_patient").val('')
}

function load_data_patient(f_name, l_name, national_code, id){
    $("#f_name").html('نام:'+' '+f_name);
    $("#l_name").html('نام خانوادگی:'+' '+l_name);
    $("#national_code").html('شماره ملی:'+' '+national_code);
    $("#id_patient").val(id);
    $("#patient_name").val(f_name+" "+l_name);
    $('.persianumber').persiaNumber();

    $('#patient_input .tt-menu').css('display', 'none');
}


jQuery(document).ready(function () {
    head_patient.init()
});

var csrf_token = "";

function delete_ward(id_ward) {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {
        var data = {
            'id_ward': id_ward,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/ward/ward/delete/',
            data: data,
            success: function (resp) {
                $('#' + id_ward).remove();
                set_count_number('count_number');
                uikitNotify('بخش شما حذف شد.', '', 'success');
            },
            error: function (e) {
                notify_error(e)
            }
        });
    });

}

function append_ward() {
    var ward_name = $("#ward_name").val();
    var id_ward = $("#id_ward").val();

    if (ward_name != "") {
        var data = {
            'ward_name': ward_name,
            'id_ward': id_ward,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/ward/ward/add/',
            data: data,
            success: function (resp) {
                if (id_ward != "") {
                    $('#' + id_ward).remove();
                    cancel_ward();
                    uikitNotify('ویرایش شد.', '', 'success');
                } else {
                    $('#all_wards').prepend(create_html_ward(resp));
                    uikitNotify('زیر بخش های خود را اضافه کنید. بخش شما اضافه شد.', '', 'success');
                }
                set_count_number('count_number');
            },
            error: function (e) {
                notify_error(e)
            }
        });
    } else {
        uikitNotify('فیلد نام خالی می باشد', '', 'danger');
    }
}

function create_html_ward(data) {
    var html =
        '<tr id="' + data["id"] + '" class="persianumber">' +
        '<td class="count_number"></td>' +
        '<td>' + data["name"] + '</td>' +
        '<td>' +
        '<a onclick="set_data_sub_ward_to_modal(\'' + data["id"] + '\')" class="btn btn-circle btn-icon-only btn-default btn  purple-plum"  data-toggle="modal" href="#add_sub_ward_modal">' +
        '<i class="icon-vector"></i>' +
        '</a>' +
        '</td>' +
        '<td class="center">' +
        '<a onclick="delete_ward(\'' + data["id"] + '\')" class="btn btn-circle btn-icon-only btn-default btn red">' +
        '<i class="icon-trash"></i>' +
        '</a>' +
        '<a onclick="set_data_ward_to_edit(\'' + data["id"] + '\',\'' + data["name"] + '\')" class="btn btn-circle btn-icon-only btn-default btn yellow">' +
        '<i class="icon-wrench"></i>' +
        '</a>' +
        '</td>' +
        '</tr>';
    return html
}

function create_html_sub_ward(data) {
    var html =
        '<tr class="persianumber" id="' + data["id"] + '">' +
        '<td class="count_number_sub"></td>' +
        '<td>' + data["name"] + '</td>' +
        '<td>' + numberWithCommas(data['price']) + '</td>' +
        '<td class="center">' +
        '<a onclick="set_data_sub_ward_to_edit(\'' + data["id"] + '\',\'' + data["name"] + '\',\'' + data["price"] + '\',\'' + data["vta_status"] + '\')" class="btn btn-circle btn-icon-only btn-default btn yellow">' +
        '<i class="icon-wrench"></i>' +
        '</a>' +
        '<a onclick="delete_sub_ward(\'' + data["id"] + '\')" class="btn btn-circle btn-icon-only btn-default btn red">' +
        '<i class="icon-trash"></i>' +
        '</a>' +
        '</td>' +
        '</tr>';
    return html
}

function set_data_sub_ward_to_modal(id_ward) {
    $('#all_sub_wards').html('');
    var data = {
        'id_ward': id_ward,
        'csrfmiddlewaretoken': csrf_token
    };
    $.ajax({
        dataType: 'json',
        type: 'POST',
        async: false,
        url: '/ward/ward/load_sub_ward/',
        data: data,
        success: function (resp) {
            $('#id_ward').val(resp["id"]);
            for (var i = 0; i < resp['sub_ward'].length; i++) {
                $('#all_sub_wards').prepend(create_html_sub_ward(resp['sub_ward'][i]));
            }
            set_count_number('count_number_sub');
            $('.persianumber').persiaNumber();

        },
        error: function (e) {
            notify_error(e)
        }
    });
}

function append_sub_ward() {
    var sub_name = $("#sub_name").val();
    var sub_price = $("#sub_price").val();
    var id_ward = $("#id_ward").val();
    var id_sub_ward = $("#id_sub_ward").val();
    var vta_status = $("#vat_status").prop("checked");

    if (sub_name != "" && sub_price != "") {
        var data = {
            'sub_name': sub_name,
            'sub_price': sub_price,
            'id_ward': id_ward,
            'id_sub_ward': id_sub_ward,
            'vta_status': vta_status,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/ward/ward/add_sub/',
            data: data,
            success: function (resp) {
                if (id_sub_ward == "") {
                    uikitNotify('زیر بخش شما اضافه شد.', '', 'success');
                } else {
                    $("#" + id_sub_ward).remove();
                    uikitNotify('ویرایش شد.', '', 'success');
                }
                $('#all_sub_wards').prepend(create_html_sub_ward(resp));
                set_count_number('count_number_sub');
                $('.persianumber').persiaNumber();
                cancel_sub_ward();
            },
            error: function (e) {
                notify_error(e)
            }
        });
    } else {
        uikitNotify('لطفا تمامی فیلد ها را وارد نمایید.', '', 'danger');
    }
}

function set_data_sub_ward_to_edit(id_sub_ward, name, price, vta_status) {
    $("#id_sub_ward").val(id_sub_ward);
    $("#sub_name").val(name);
    $("#sub_price").val(price);
    $("#button_sub_ward_submit").html('<i class="fa fa-edit"></i>' + 'ویرایش ');
    $("#button_sub_ward_cancel").css('display', 'block');
    if (vta_status == "true") {
        $("#vat_status").prop("checked", true)
    } else {
        $("#vat_status").prop("checked", false)
    }


}
function cancel_sub_ward() {
    $("#id_sub_ward").val('');
    $("#sub_name").val('');
    $("#sub_price").val('');
    $("#button_sub_ward_submit").html('<i class="fa fa-plus"></i>' + 'افزودن');
    $("#button_sub_ward_cancel").css('display', 'none');

}
function set_data_ward_to_edit(id_ward, name) {
    $("#id_ward").val(id_ward);
    $("#ward_name").val(name);
    $("#button_ward_submit").html('<i class="fa fa-edit"></i>' + 'ویرایش');
    $("#button_ward_cancel").css('display', 'block');

}
function cancel_ward() {
    $("#id_ward").val('');
    $("#ward_name").val('');
    $("#button_ward_submit").html('<i class="fa fa-plus"></i>' + 'افزودن');
    $("#button_ward_cancel").css('display', 'none');

}

function delete_sub_ward(id_sub_ward) {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {

        var id_ward = $("#id_ward").val();
        var data = {
            'id_sub': id_sub_ward,
            'id_ward': id_ward,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/ward/ward/delete_sub/',
            data: data,
            success: function (resp) {
                $('#' + id_sub_ward).remove();
                set_count_number('count_number_sub');
                uikitNotify('پاک شد', '', 'success');
            },
            error: function (e) {
                notify_error(e)
            }
        });
    });
}

function set_count_number(selector) {
    var count_number = 1;
    $('.' + selector).each(function () {
        $(this).html(count_number);
        count_number += 1;
    });
}


$("#sub_price").on("keypress", function (event) {
    var englishAlphabetAndWhiteSpace = /^([0-9])$/;
    var key = String.fromCharCode(event.which);
    if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
        return true;
    }
    uikitNotify('عدد انگلیسی وارد کنید.', '', 'danger');
    return false;
});
$('#sub_price').on("paste", function (e) {
    e.preventDefault();
});


$('#add_sub_ward_modal').on('hide.bs.modal', function (e) {
    cancel_ward();
});
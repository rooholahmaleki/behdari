function active_menu_item(selector_page_item) {
    $(selector_page_item).addClass('start');
    $(selector_page_item).addClass('active');
    $(selector_page_item).addClass('open');
}

function display_block_sub_menu(selector_page_item) {
    $(selector_page_item).css('display', 'block')
}

function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function notify_error(error) {
    if (error.status == 403) {
        if (error.responseText == "Forbidden") {
            uikitNotify('شما دسترسی به این عمل را ندارید.', '', 'warning');
        } else {
            uikitNotify('csrf_token', '', 'warning');
        }

    } else if (error.status == 500) {
        uikitNotify('با عرض پوزش سیستم با مشکل رو به رو شده است.', '', 'danger');

    } else if (error.status == 404) {
        uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', '', 'danger');
    }
}

function startTime() {
    var x = persianDate().format("dddd , DD MMMM YYYY | H:mm:ss");
    $('#tm-date-and-time').text(x);

    t = setTimeout(function () {
        startTime();
    }, 1000);
}
startTime();

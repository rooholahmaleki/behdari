var ComponentsTypeahead = function () {
    var e = function () {
        $("#name_medicine").attr("dir", "rtl"),
        $("#name_medicine").typeahead(null, {
            source: function (query) {
                return $.get('/finance/autocomplete/medicine/', { medicine: query }, function (data) {
                    for(var item in data){
                        var html = '<div class="tt-suggestion tt-selectable" ' +
                            ' onclick="load_data_medicine(\''+data[item]['en_name']+'\',\''+data[item]['fa_name']+'\', \''+data[item]['price']+'\', \''+data[item]['count']+'\', \''+data[item]['id']+'\')">'+data[item]['en_name']+'( '+data[item]['fa_name']+' )</div>';
                        $('#medicine_input .tt-menu').removeClass('tt-empty');
                        $('#medicine_input .tt-menu').css('display', 'block');
                        $('#medicine_input .tt-dataset').append(html);
                        $("#en_name_medicine_label").html('نام:'+' '+data[item]['en_name']+'('+data[item]['fa_name']+')');
                        $("#price_medicine_label").html('قیمت(ریال):'+' '+numberWithCommas(data[item]['price']));
                        $("#count_medicine_label").html('موجودی:'+' '+data[item]['count']);
                        $("#id_medicine").val(data[item]['id'])
                    }
                    if(data.length == 0){
                        var html = '<div class="tt-suggestion tt-selectable">چیزی یافت نشد</div>';
                        $('#medicine_input .tt-dataset').append(html);
                        $('#medicine_input .tt-menu').removeClass('tt-empty');
                        $('#medicine_input .tt-menu').css('display', 'block');
                        clean_input_html();

                    }
                });
            }
        });
    };
    return {
        init: function () {
            e()
        }
    }
}();
jQuery(document).ready(function () {
    ComponentsTypeahead.init()
});

function clean_input_html(){
    $("#en_name_medicine_label").html('نام  :');
    $("#price_medicine_label").html('قیمت(ریال)  :0');
    $("#count_medicine_label").html('موجودی:0');
    $("#id_medicine").val('');
    $("#count_medicine").val('');
}

function load_data_medicine(en_name, fa_name, price, count, id){
    $("#en_name_medicine_label").html('نام:'+' '+en_name+'('+fa_name+')');
    $("#price_medicine_label").html('قیمت(ریال):'+' '+numberWithCommas(price));
    $("#count_medicine_label").html('موجودی:'+' '+count);
    $("#id_medicine").val(id);
    $("#name_medicine").val(en_name+"("+fa_name+")");
    $('.persianumber').persiaNumber();

    $('#medicine_input .tt-menu').css('display', 'none');
}

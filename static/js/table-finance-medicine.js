var TableDatatablesButtons = function () {
    var e = function () {
        var e = $("#table_finance");
        e.dataTable({
            language: {
                aria: {
                    sortAscending: ": مرتب سازی صعودی",
                    sortDescending: ": مرتب سازی نزولی"
                },
                emptyTable: "هیچ فاکتوری وجود ندارد",
                info: " _START_ تا _END_ از _TOTAL_ فاکتور",
                infoEmpty: "",
                infoFiltered: "",
                lengthMenu: "تعداد جهت نمایش :  _MENU_",
                search: "جستجو:",
                zeroRecords: "هیچ چیزی یافت نشد"
            },
            buttons: [],
            responsive: {details: {}},
            order: [[0, "asc"]],
            lengthMenu: [[5, 10, 20, 40, 80, -1], [5, 10, 20, 40, 80, "همه"]],
            pageLength: 10,
            dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
        })
    }, validation_form = function () {
        var e = $("#add_finance"), r = $(".alert-danger", e), i = $(".alert-success", e);
        e.validate({
            errorElement: "span",
            errorClass: "help-block help-block-error",
            focusInvalid: !1,
            ignore: "",
            rules: {
                fa_name: {required: !0},
                en_name: {required: !0},
                start_date: {date: !0, required: !0},
                end_date: {date: !0, required: !0},
                count: {number: !0,required: !0},
                price: {number: !0,required: !0},
                count_type: {required: !0}
            },
            invalidHandler: function (e, t) {
                i.hide(), r.show(), App.scrollTo(r, -200)
            },
            highlight: function (e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function (e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
            success: function (e) {
                e.closest(".form-group").removeClass("has-error")
            },
            submitHandler: function (e) {
                i.show(), r.hide()
            }
        })
    };
    return {
        init: function () {
            jQuery().dataTable && (e(), validation_form())
        }
    }
}();
jQuery(document).ready(function () {
    TableDatatablesButtons.init();
});
var csrf_token = "";

function delete_patient(id_patient) {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {
        var data = {
            'id_patient': id_patient,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/patient/patient/delete/',
            data: data,
            success: function (resp) {
                $('#' + id_patient).remove();
                uikitNotify('کاربر شما پاک شد.', '', 'success');
                set_count_number();
            },
            error: function (e) {
                notify_error(e)
            }
        });
    });

}
$('#add_patient').submit(function (e) {
    e.preventDefault();
    if ($("#f_name").val() != "" && $("#l_name").val() != ""){
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/patient/patient/add/',
            data: $(this).serializeArray(),
            success: function (resp) {
                console.log(resp);
                if(resp['data']['message'] != 'repeat_national_code'){
                    var id_patient = $('#add_patient input[name="id_patient"]').val();
                    if (id_patient == "") {
                        uikitNotify('کاربر جدید اضافه شد.', '', 'success');
                    } else {
                        $("#" + id_patient).remove();
                        uikitNotify('کاربر ویرایش شد.', '', 'success');
                    }
                    $('#all_patients').prepend(create_html_patient(resp['data']));
                    set_count_number();
                    clear_input();
                }else{
                    uikitNotify('این کد ملی قبلا استفاده شده است.', '', 'danger');
                }
            },
            error: function (e) {
                notify_error(e)
            }
        });
    }else{
        uikitNotify('نام و نام خانوادگی را وارد کنید.', '', 'danger');
    }
});

function clear_input(){
    $('#add_patient input[name="id_patient"]').val('');
    $('#add_patient input[name="f_name"]').val('');
    $('#add_patient input[name="l_name"]').val('');
    $('#add_patient input[name="national_code"]').val('0');
}

function create_html_patient(data){
    var html =
        '<tr id="'+data['id']+'">'+
            '<td class="count_number"></td>'+
            '<td>'+data['full_name']+'</td>'+
            '<td>'+data['national_code']+'</td>'+
            '<td class="center">'+
                '<a onclick="delete_patient(\''+data['id']+'\')" class="btn btn-circle btn-icon-only btn-default btn red">'+
                    '<i class="icon-trash"></i>'+
                '</a>'+
                '<a onclick="edit_patient(\''+data['id']+'\')" class="btn btn-circle btn-icon-only btn-default btn green"  data-toggle="modal" href="#add_patient_modal">'+
                    '<i class="icon-wrench"></i>'+
                '</a>'+
            '</td>'+
        '</tr>';
    return html
}

function edit_patient(id_patient) {
    var data = {
        'id_patient': id_patient,
        'csrfmiddlewaretoken': csrf_token
    };
    $.ajax({
        dataType: 'json',
        type: 'POST',
        async: false,
        url: '/patient/patient/edit/',
        data: data,
        success: function (resp) {
            $('#add_patient input[name="id_patient"]').val(resp['id']);
            $('#add_patient input[name="f_name"]').val(resp['f_name']);
            $('#add_patient input[name="l_name"]').val(resp['l_name']);
            $('#add_patient input[name="national_code"]').val(resp['national_code']);
        },
        error: function (e) {
            notify_error(e)
        }
    });
}

function set_count_number(){
    var count_number = 1;
    $('.count_number').each(function(){
        $(this).html(count_number);
        count_number += 1;
    });
}
var TableDatatablesButtons = function () {
    var e = function () {

    }, validation_form = function () {
        var e = $("#add_medicine"), r = $(".alert-danger", e), i = $(".alert-success", e);
        e.validate({
            errorElement: "span",
            errorClass: "help-block help-block-error",
            focusInvalid: !1,
            ignore: "",
            rules: {
                fa_name: {required: !0},
                en_name: {required: !0},
                start_date: {date: !0, required: !0},
                end_date: {date: !0, required: !0},
                // count: {number: !0,required: !0},
                price: {number: !0,required: !0},
                // count_type: {required: !0}
            },
            invalidHandler: function (e, t) {
                i.hide(), r.show(), App.scrollTo(r, -200)
            },
            highlight: function (e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function (e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
            success: function (e) {
                e.closest(".form-group").removeClass("has-error")
            },
            submitHandler: function (e) {
                i.show(), r.hide()
            }
        })
    };
    return {
        init: function () {
            jQuery().dataTable && (e(), validation_form())
        }
    }
}();
jQuery(document).ready(function () {
    TableDatatablesButtons.init();
});
var csrf_token = "";
var option = "";

function delete_user(id_user) {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {
        var data = {
            'id_user': id_user,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/user/user/delete/',
            data: data,
            success: function (resp) {
                $('#' + id_user).remove();
                uikitNotify('کاربر شما پاک شد.', '', 'success');
            },
            error: function (e) {
                notify_error(e)
            }
        });
    });

}

$('#add_user').submit(function () {
    if ($(this).valid() && check_password()) {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: false,
            url: '/user/user/add/',
            data: $(this).serializeArray(),
            success: function (resp) {
                if (resp['message'] == 'success') {
                    $('.close_modal_footer').click();
                    var id_user = $('#add_user input[name="id_user"]').val();
                    if (id_user == "") {
                        uikitNotify('کاربر جدید اضافه شد.', '', 'success');
                    } else {
                        $("#" + id_user).remove();
                        uikitNotify('کاربر ویرایش شد.', '', 'success');
                    }
                    $('#all_users').append(create_html_user(resp['data']));
                    set_count_number();
                } else {
                    uikitNotify('نام کاربری دیگری انتخاب کنید', '', 'danger');
                }
            },
            error: function (e) {
                notify_error(e)
            }
        });
    }
});

function create_html_user(data) {
    var html =
        '<tr id="' + data['id'] + '">' +
        '<td class="count_number"></td>' +
        '<td>' + data['full_name'] + '</td>' +
        '<td>' + data['user_type'] + '</td>' +
        '<td class="center">' +
        '<a onclick="delete_user(\'' + data['id'] + '\')" class="btn btn-circle btn-icon-only btn-default btn red">' +
        '<i class="icon-trash"></i>' +
        '</a>' +
        '<a onclick="set_data_user_to_modal(\'' + data['id'] + '\')" class="btn btn-circle btn-icon-only btn-default btn green"  data-toggle="modal" href="#add_user_modal">' +
        '<i class="icon-wrench"></i>' +
        '</a>' +
        '</td>' +
        '</tr>';
    return html
}

function set_count_number() {
    var count_number = 1;
    $('.count_number').each(function () {
        $(this).html(count_number);
        count_number += 1;
    });
}

function check_password() {
    if ($('#password').val() != $('#rep_password').val()) {
        uikitNotify('پسووردها مطابقت ندارند', '', 'danger');
        return false;
    }
    return true;
}

function set_data_user_to_modal(id_user) {
    var data = {
        'id_user': id_user,
        'csrfmiddlewaretoken': csrf_token
    };
    $.ajax({
        dataType: 'json',
        type: 'POST',
        async: false,
        url: '/user/user/edit/',
        data: data,
        success: function (resp) {
            $('#add_user input[name="id_user"]').val(resp['id']);
            $('#add_user input[name="f_name"]').val(resp['f_name']);
            $('#add_user input[name="l_name"]').val(resp['l_name']);
            $('#add_user input[name="user_name"]').val(resp['user_name']);
            $('#submit_modal').html('ویرایش');
            $('#title_modal').html('ویرایش  کاربر')
        },
        error: function (e) {
            notify_error(e)
        }
    });
}


var validation_form = function () {
    var validation_form = function () {
        var e = $("#add_user"), r = $(".alert-danger", e), i = $(".alert-success", e);
        e.validate({
            errorElement: "span",
            errorClass: "help-block help-block-error",
            focusInvalid: !1,
            ignore: "",
            rules: {
                f_name: {required: !0},
                l_name: {required: !0},
                password: {required: !0},
                rep_password: {required: !0},
                user_type: {required: !0},
                user_name: {required: !0}
            },
            invalidHandler: function (e, t) {
                i.hide(), r.show(), App.scrollTo(r, -200)
            },
            highlight: function (e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function (e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
            success: function (e) {
                e.closest(".form-group").removeClass("has-error")
            },
            submitHandler: function (e) {
                i.show(), r.hide()
            }
        })
    };
    return {
        init: function () {
            jQuery().dataTable && (validation_form())
        }
    }
}();
jQuery(document).ready(function () {
    validation_form.init();
});

$('.close_modal_footer').click(function () {
    $('.close_modal').click();
});

$("#button_user_modal").click(function () {
    $('.close_modal_footer').click();
    $('#submit_modal').html('افزودن');
    $('#title_modal').html('افزودن کاربر');
    $('#add_user input[name="id_user"]').val('');
});
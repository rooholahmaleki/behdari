var csrf_token,
    count = 1,
    list_sub_ward = [],
    id_ward_selected = "",
    price_sub_ward = "",
    vta_status_sub_ward = "false",
    sum_price = 0,
    sum_price_vta = 0,
    id_sub_ward_selected = "";

function load_sub_ward() {
    $("#sub_ward_select").html('');
    var id_ward = $("#ward_select").val();
    id_ward_selected = id_ward;
    if (id_ward != "none") {
        $.ajax({
            url: '/finance/load/sub_ward/',
            type: 'POST',
            async: false,
            data: {
                'id_ward': id_ward,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (resp) {
                $("#sub_ward_select").append('<option value="none">انتخاب کنید</option>');
                id_sub_ward_selected = "";
                for (var i in resp['sub_ward']) {
                    var sub_ward = resp['sub_ward'][i];
                    var option =
                        '<option data-vta="' + sub_ward['vta_status'] + '" data-price="' + sub_ward['price'] + '" value="' + sub_ward['id'] + '">' +
                        sub_ward['name'] +
                        '</option>';
                    $("#sub_ward_select").append(option)
                }
            }
        });
    } else {
        $("#sub_ward_select").html('<option value="none">انتخاب کنید</option>');
        $("#price_medicine_label").html('قیمت(ریال): ');
        id_ward_selected = "";
        id_sub_ward_selected = "";
    }
}

function load_sub_price_ward() {
    if ($("#sub_ward_select").val() != "none") {
        price_sub_ward = $("#sub_ward_select option:selected").attr('data-price');
        vta_status_sub_ward = $("#sub_ward_select option:selected").attr('data-vta');
        id_sub_ward_selected = $("#sub_ward_select").val();
        $("#price_medicine_label").html('قیمت(ریال): ' + numberWithCommas(price_sub_ward));
        $('.persianumber').persiaNumber();
    } else {
        $("#price_medicine_label").html('قیمت(ریال): ');
        id_sub_ward_selected = "";
    }
}

function add_sub_ward() {
    var count_sub_ward = $("#count_sub_ward").val();
    if (check_is_not_added()) {
        if (count_sub_ward != "" && id_sub_ward_selected != "" && id_ward_selected != "") {
            var dict_sub_ward = {
                "id": guid(),
                "count_sub_ward": count_sub_ward,
                "id_ward": id_ward_selected,
                "vta_status": vta_status_sub_ward,
                "id_sub_ward": id_sub_ward_selected
            };
            list_sub_ward.push(dict_sub_ward);
            price_sub_ward = parseInt(dict_sub_ward['count_sub_ward']) * parseInt(price_sub_ward);
            append_sum_price();
            append_html_ward_to_table(dict_sub_ward);
            clear_all_input_ward();
            set_count_number();
            $('.persianumber').persiaNumber();
        } else {
            uikitNotify('همه فیلد های لازم باید پر باشند', '', 'danger');
        }
    } else {
        uikitNotify('این خدمت قبلا اضافه شده است.', '', 'warning');
    }
}

function check_is_not_added() {
    for (var i in list_sub_ward) {
        if (list_sub_ward[i]['id_ward'] == id_ward_selected && list_sub_ward[i]['id_sub_ward'] == id_sub_ward_selected) {
            return false
        }
    }
    return true
}

function append_sum_price() {
    sum_price += price_sub_ward;
    $("#sum-price").html(numberWithCommas(sum_price));
    $("#received_price").val(sum_price);

    if (vta_status_sub_ward == "true") {
        sum_price_vta += (price_sub_ward * (9 / 100));
        $("#sum-price-vta").html(numberWithCommas(sum_price_vta))
    }

    $("#total-amount").html(numberWithCommas(sum_price_vta + sum_price))
}

function clear_all_input_ward() {
    $("#count_sub_ward").val(1);
    $("#price_medicine_label").html('قیمت(ریال) :')
}

function append_html_ward_to_table(dict_sub_ward) {
    var html_ward = $("#ward_select option[value=" + dict_sub_ward['id_ward'] + "]").html();
    var html_sub_ward = $("#sub_ward_select option[value=" + dict_sub_ward['id_sub_ward'] + "]").html();
    var ward_html =
        '<tr id="' + dict_sub_ward['id'] + '" class="visit_finance persianumber" data-price="' + price_sub_ward + '" data-vta="' + vta_status_sub_ward + '">' +
        '<td class="count_number"></td>' +
        '<td>' + html_ward + '</td>' +
        '<td>' + html_sub_ward + '</td>' +
        '<td>' + dict_sub_ward['count_sub_ward'] + '</td>' +
        '<td>' + ((vta_status_sub_ward == "true") ? "دارد" : "ندارد") + '</td>' +
        '<td>' +
        '<a onclick="delete_sub_ward(\'' + dict_sub_ward['id'] + '\')" class="btn btn-circle btn-icon-only btn-default btn red">' +
        '<i class="icon-trash"></i>' +
        '</a>' +
        '</td>' +
        '<td>' + numberWithCommas(price_sub_ward) + '</td>' +
        '</tr>';
    $("#all_medicine > tr:first ").before(ward_html);
}

function delete_sub_ward(id) {
    var price = $("#" + id).attr('data-price');
    var vta_status_sub_ward = $("#" + id).attr('data-vta');
    sum_price -= parseInt(price);
    $("#sum-price").html(numberWithCommas(sum_price));
    $("#received_price").val(sum_price);

    if (vta_status_sub_ward == "true") {
        sum_price_vta -= ((price * 9) / 100);
        $("#sum-price-vta").html(numberWithCommas(sum_price_vta))
    }

    $("#total-amount").html(numberWithCommas(sum_price_vta + sum_price));
    list_sub_ward = $.grep(list_sub_ward, function (e) {
        return e.id != id;
    });
    $("#" + id).remove();
    $('.persianumber').persiaNumber();
}

function add_finance() {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {
        var received_price = $('#received_price').val();
        var patient_name = $('#patient_name').val();
        if (received_price != "" && list_sub_ward.length > 0 && patient_name != "") {
            if ($("#id_patient").val() == "") {
                id_patient = 'guest';
            } else {
                id_patient = $("#id_patient").val();
            }
            var data = {
                'csrfmiddlewaretoken': csrf_token,
                'received_price': received_price,
                'patient_name': patient_name,
                'list_sub_ward': JSON.stringify(list_sub_ward),
                'id_patient': id_patient
            };
            $.ajax({
                dataType: 'json',
                type: 'POST',
                async: false,
                url: '/finance/add/finance/visit/',
                data: data,
                success: function (resp) {
                    if (resp['message'] == "success") {
                        window.open(
                            '/finance/print/finance/visit/' + resp['id_visit'],
                            '_blank'
                        );
                        location.reload();

                    } else if (resp['message'] == "empty_factor") {
                        uikitNotify('هیچ خدماتی ارایه نشده است', '', 'danger');

                    } else if (resp['message'] == "wrong_received_price") {
                        uikitNotify('مبلغ دریافتی نامعتبر می باشد.', '', 'danger');
                    }
                },
                error: function (e) {
                    notify_error(e)
                }
            });
        } else {
            if (list_sub_ward.length <= 0) {
                uikitNotify('هیچ خدماتی ارایه نشده است.', '', 'danger');
            } else if (received_price == "") {
                uikitNotify('مبلغ دریافتی را وارد کنید.', '', 'danger');
            } else {
                uikitNotify('نام بیمار را وارد کنید', '', 'danger');
            }
        }
    });
}

function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function set_count_number() {
    var count_number = 1;
    $('.count_number').each(function () {
        $(this).html(count_number);
        count_number += 1;
    });
}

$("#count_sub_ward").on("keypress", function (event) {
    var englishAlphabetAndWhiteSpace = /^([0-9])$/;
    var key = String.fromCharCode(event.which);
    if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
        return true;
    }
    uikitNotify('عدد انگلیسی وارد کنید.', '', 'danger');
    return false;
});
$('#count_sub_ward').on("paste", function (e) {
    e.preventDefault();
});

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4();
}

var csrf_token = "";
var count_selected_medicine = 0;

var option = "";
for (var i = 2; i < 100; i++) {
    option += '<option value="' + i + '">' + i + '</option>'
}

var count_package_html =
    '<div class="form-group form-md-line-input load_multiplex">' +
    '<label class="col-md-3 control-label">چندتایی</label>' +
    '<div class="col-md-9">' +
    '<select class="beh-set-height-dropdown form-control" name="count_package">' +
    option +
    '</select>' +
    '</div>' +
    '<div class="form-control-focus"></div>' +
    '</div>';


var medicine_table = $("#medicine").DataTable({
    language: {
        aria: {
            sortAscending: ": مرتب سازی صعودی",
            sortDescending: ": مرتب سازی نزولی"
        },
        emptyTable: "هیچ دارویی وجود ندارد.",
        info: " _START_ تا _END_ از _TOTAL_ دارو",
        infoEmpty: "",
        infoFiltered: "",
        lengthMenu: "تعداد جهت نمایش :  _MENU_",
        search: "جستجو:",
        zeroRecords: "هیچ چیزی یافت نشد"
    },

    buttons: [
        {extend: "print", className: "btn dark btn-outline", text: "پرینت"},
        {extend: "excel", className: "btn blue btn-outline", text: "اکسل"},
        {extend: "colvis", className: "btn red btn-outline", text: "حذف ستون ها"}
    ],
    responsive: {details: {}},
    order: [[0, "asc"]],
    lengthMenu: [[5, 10, 20, 40, 80, -1], [5, 10, 20, 40, 80, "همه"]],
    pageLength: 10,
    dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
})


function delete_medicine(id_medicine) {
    UIkit.modal.confirm("آیا می خواهید ادامه دهید؟", function () {
        var data = {
            'id_medicine': id_medicine,
            'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/medicine/medicines/delete/',
            data: data,
            success: function (resp) {
                medicine_table.row($("#" + id_medicine)).remove();
                $('#' + id_medicine).remove();
                uikitNotify('داروی شما پاک شد.', '', 'success');

            },
            error: function (e) {
                notify_error(e)
            }
        });
    });

}

function load_multiplex(select, position) {
    if ($(select).val() == 'package') {
        if (position == "count_modal") {
            $('#modal_dive').append(count_package_html);

        } else {
            $('#div_right').append(count_package_html);
        }
    } else {
        $('.load_multiplex').remove();
    }
}

$('#add_medicine').submit(function (e) {
    e.preventDefault();
    var _id = $('#add_medicine input[name="id_medicine"]').val();
    var start_date = $('#start_date').val().split('/');
    var end_date = $('#end_date').val().split('/');
    start_date = check_date(start_date);
    end_date = check_date(end_date);
    if ($(this).valid() && start_date && end_date) {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: '/medicine/medicines/add/',
            data: $(this).serializeArray(),
            success: function (resp) {
                if (resp['message'] == 'success') {
                    medicine_table.row($("#" + _id)).remove();
                    $("#" + _id).remove();

                    $('.close_modal_footer').click();
                    create_html_medicine(resp);

                } else {
                    uikitNotify('سیستم با مشکل روبه رو شده است.', '', 'danger');
                }
            },
            error: function (e) {
                notify_error(e)
            }
        });
    }
});

$('.close_modal_footer').click(function () {
    $('.close_modal').click();
    $('.load_multiplex').remove();
    $("#id_medicine_for_edit_count").val('');
    $('#add_medicine input[name="id_medicine"]').val("")
});

function change_html_help_block() {
    var html = "";
    if ($("#date_type").val() == 'helical') {
        html = "تاریخ  . به عنوان مثال: 1395/06/02";
    } else {
        html = "تاریخ  . به عنوان مثال: 2015/06/02";
    }
    $('.date_medicine .help-block').each(function () {
        $(this).html(html)
    });
}

function check_date(date) {
    try {
        var year = parseInt(date[0]),
            month = parseInt(date[1]),
            day = parseInt(date[2]),
            date_type = $("#date_type").val();

        if (date_type == 'helical') {
            if (1300 < year && year < 1900 && 0 < month && month < 13 && 0 < day && day < 32) {
                return true
            }
            uikitNotify('تاریخ ها درست وارد نشده اند', '', 'danger');
            return false;
        } else if (date_type == 'gregorian') {
            if (1900 < year && year < 2300 && 0 < month && month < 13 && 0 < day && day < 32) {
                return true
            }
            uikitNotify('تاریخ ها درست وارد نشده اند', '', 'danger');
            return false;
        }
        uikitNotify('تاریخ ها درست وارد نشده اند', '', 'danger');
        return false;

    } catch (e) {
        uikitNotify('تاریخ ها درست وارد نشده اند', '', 'danger');
        return false
    }
}

function create_html_medicine(resp) {
    var but = '<a onclick="delete_medicine(\'' + resp["id"] + '\')" class="btn btn-circle btn-icon-only btn-default btn red">' +
        '<i class="icon-trash"></i>' +
        '</a>' +
        '<a onclick="set_data_to_modal(\'' + resp["id"] + '\')" class="btn btn-circle btn-icon-only btn-default btn yellow" data-toggle="modal" href="#add_medicine_modal">' +
        '<i class="icon-wrench"></i>' +
        '</a>' +
        '<a onclick="set_id_medicine(\'' + resp["id"] + '\')" class="btn btn-circle btn-icon-only btn-default btn green" data-toggle="modal" href="#add_medicine_modal">' +
        '<i class="icon-basket-loaded"></i>' +
        '</a>';

    var node = medicine_table.row.add([
        1,
        resp["fa_name"],
        resp["en_name"],
        resp["dict_date"]['start_date'],
        resp["dict_date"]['end_date'],
        ((resp["count"]) ? resp["count"] : count_selected_medicine),
        resp["price"],
        but
    ]).draw(true).node();

    $(node).attr("id", resp["id"])
}

function set_data_to_modal(id_medicine) {
    var data = {
        'id_medicine': id_medicine,
        'csrfmiddlewaretoken': csrf_token
    };
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: '/medicine/medicines/edit/',
        data: data,
        success: function (resp) {
            if (!$('#count_medicine').hasClass("hidden")) {
                $('#count_medicine').addClass('hidden');
            }
            count_selected_medicine = resp['count'];
            $('#add_medicine input[name="id_medicine"]').val(resp['id']);
            $('#add_medicine input[name="fa_name"]').val(resp['fa_name']);
            $('#add_medicine input[name="en_name"]').val(resp['en_name']);
            $('#add_medicine input[name="price"]').val(resp['price']);
            $('#add_medicine input[name="start_date"]').val(resp['dict_date']['start_date']);
            $('#add_medicine input[name="end_date"]').val(resp['dict_date']['end_date']);
            if (resp['dict_date']['persian_date'] == false) {
                $('#add_medicine select[name="date_type"] option[value="gregorian"]').attr('selected', 'selected')
            }
            $('#submit_modal').html('ویرایش ');
            $('#title_modal').html('ویرایش دارو')
        },
        error: function (e) {
            notify_error(e)
        }
    });
}

function clear_modal_medicine() {
    $('.close_modal_footer').click();
    $('#submit_modal').html('افزودن');
    $('#title_modal').html('افزودن دارو');
    $('#add_medicine input[name="id_medicine"]').val('');
    $('#count_medicine').removeClass('hidden');
}

function add_count_medicine() {
    var id_medicine = $('#id_medicine_for_edit_count').val();
    var count = $('#count_medicine_input').val();
    var count_package = $('select[name="count_package"]').val();
    var data = {
        'csrfmiddlewaretoken': csrf_token,
        'id_medicine': id_medicine,
        'count': count,
        'count_package': count_package
    };
    $.ajax({
        dataType: 'json',
        type: 'POST',
        async: false,
        url: '/medicine/medicines/add_count/',
        data: data,
        success: function (resp) {
            if (resp["message"] == "success") {
                $('.close_modal_footer').click();
                uikitNotify('تعداد اضافه شد.', '', 'success');
            } else {
                uikitNotify('تعداد دارو تغییر نکرد.', '', 'danger');
            }
        },
        error: function (e) {
            notify_error(e)
        }
    });
}

function set_id_medicine(id_medicine) {
    $("#id_medicine_for_edit_count").val(id_medicine)
}